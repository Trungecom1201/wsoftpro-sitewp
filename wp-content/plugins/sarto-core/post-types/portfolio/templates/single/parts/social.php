<?php if ( sarto_edge_core_plugin_installed() && sarto_edge_options()->getOptionValue( 'enable_social_share' ) == 'yes' && sarto_edge_options()->getOptionValue( 'enable_social_share_on_portfolio-item' ) == 'yes' ) : ?>
	<div class="edgtf-ps-info-item edgtf-ps-social-share">
		<?php
		/**
		 * Available params type, icon_type and title
		 *
		 * Return social share html
		 */
		echo sarto_edge_get_social_share_html( array( 'type'  => 'list', 'title' => esc_attr__( 'Follow me:', 'sarto-core' ) ) ); ?>
	</div>
<?php endif; ?>