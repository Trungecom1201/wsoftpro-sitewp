<?php
$masonry_classes = '';
$number_of_columns = sarto_edge_get_meta_field_intersect('portfolio_single_masonry_columns_number');
if(!empty($number_of_columns)) {
	$masonry_classes .= ' edgtf-ps-'.$number_of_columns.'-columns';
}
$space_between_items = sarto_edge_get_meta_field_intersect('portfolio_single_masonry_space_between_items');
if(!empty($space_between_items)) {
	$masonry_classes .= ' edgtf-'.$space_between_items.'-space';
}
?>
<div class="edgtf-grid-row">
	<div class="edgtf-grid-col-9">
		<div class="edgtf-ps-image-holder edgtf-ps-masonry-images <?php echo esc_attr($masonry_classes); ?>">
			<div class="edgtf-ps-image-inner edgtf-outer-space">
				<div class="edgtf-ps-grid-sizer"></div>
				<div class="edgtf-ps-grid-gutter"></div>
				<?php
				$media = sarto_core_get_portfolio_single_media();
				
				if(is_array($media) && count($media)) : ?>
					<?php foreach($media as $single_media) : ?>
						<div class="edgtf-ps-image edgtf-item-space <?php echo esc_attr($single_media['holder_classes']); ?>">
							<?php sarto_core_get_portfolio_single_media_html($single_media); ?>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
    <div class="edgtf-grid-col-3">
        <div class="edgtf-ps-info-holder edgtf-ps-info-sticky-holder">
            <?php
            //get portfolio content section
            sarto_core_get_cpt_single_module_template_part('templates/single/parts/content', 'portfolio', $item_layout);?>


            <div class="edgtf-ps-info-holder-inner">
                <h5><?php esc_html_e( 'Info', 'sarto-core' ); ?></h5>
                <?php

                //get portfolio custom fields section
                sarto_core_get_cpt_single_module_template_part('templates/single/parts/custom-fields', 'portfolio', $item_layout);

                //get portfolio categories section
                sarto_core_get_cpt_single_module_template_part('templates/single/parts/categories', 'portfolio', $item_layout);

                //get portfolio date section
                sarto_core_get_cpt_single_module_template_part('templates/single/parts/date', 'portfolio', $item_layout);

                //get portfolio tags section
                sarto_core_get_cpt_single_module_template_part('templates/single/parts/tags', 'portfolio', $item_layout);

                //get portfolio share section
                sarto_core_get_cpt_single_module_template_part('templates/single/parts/social', 'portfolio', $item_layout);
                ?>
            </div>
        </div>
    </div>
    <div class="edgtf-grid-col-12">
        <?php
        //get portfolio categories section
        sarto_core_get_cpt_single_module_template_part('templates/single/parts/related-posts', 'portfolio', $item_layout);
        ?>
    </div>
</div>