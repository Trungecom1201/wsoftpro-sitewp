<div class="edgtf-ps-image-holder">
	<div class="edgtf-ps-image-inner edgtf-owl-slider">
		<?php
		$media = sarto_core_get_portfolio_single_media();
		
		if(is_array($media) && count($media)) : ?>
			<?php foreach($media as $single_media) : ?>
				<div class="edgtf-ps-image">
					<?php sarto_core_get_portfolio_single_media_html($single_media); ?>
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
</div>
<div class="edgtf-grid-row">
	<div class="edgtf-grid-col-9">
		<?php sarto_core_get_cpt_single_module_template_part('templates/single/parts/content', 'portfolio', $item_layout); ?>
	</div>
    <div class="edgtf-grid-col-3">
        <div class="edgtf-ps-info-holder">

            <div class="edgtf-ps-info-holder-inner">
                <h5><?php esc_html_e( 'Info', 'sarto-core' ); ?></h5>
                <?php

                //get portfolio custom fields section
                sarto_core_get_cpt_single_module_template_part('templates/single/parts/custom-fields', 'portfolio', $item_layout);

                //get portfolio categories section
                sarto_core_get_cpt_single_module_template_part('templates/single/parts/categories', 'portfolio', $item_layout);

                //get portfolio date section
                sarto_core_get_cpt_single_module_template_part('templates/single/parts/date', 'portfolio', $item_layout);

                //get portfolio tags section
                sarto_core_get_cpt_single_module_template_part('templates/single/parts/tags', 'portfolio', $item_layout);

                //get portfolio share section
                sarto_core_get_cpt_single_module_template_part('templates/single/parts/social', 'portfolio', $item_layout);
                ?>
            </div>
        </div>
    </div>
    <div class="edgtf-grid-col-12">
        <?php
        //get portfolio categories section
        sarto_core_get_cpt_single_module_template_part('templates/single/parts/related-posts', 'portfolio', $item_layout);
        ?>
    </div>
</div>