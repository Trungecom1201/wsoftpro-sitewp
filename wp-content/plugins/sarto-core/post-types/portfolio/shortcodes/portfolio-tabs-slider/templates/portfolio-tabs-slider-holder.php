<div class="edgtf-portfolio-tabs-slider <?php esc_attr( $params['holder_classes'] ) ?>">
	<?php echo sarto_core_get_cpt_shortcode_module_template_part( 'portfolio', 'portfolio-tabs-slider', 'portfolio-tabs-slider-categories', '', $params ); ?>
    <div class="edgtf-pts-content edgtf-owl-slider">
		<?php foreach ( $categories_array as $category ) {
			$params['category'] = $category;
			echo sarto_core_get_cpt_shortcode_module_template_part( 'portfolio', 'portfolio-tabs-slider', 'portfolio-tabs-slider-items', '', $params );
		} ?>
    </div>
</div>