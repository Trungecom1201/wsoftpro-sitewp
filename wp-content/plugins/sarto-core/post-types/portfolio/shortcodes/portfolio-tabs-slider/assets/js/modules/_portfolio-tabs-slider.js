(function($) {
    'use strict';

    var portfolioTabSlider = {};
    edgtf.modules.portfolioTabSlider = portfolioTabSlider;

    portfolioTabSlider.edgtfOnDocumentReady = edgtfOnDocumentReady;
    portfolioTabSlider.edgtfOnWindowLoad = edgtfOnWindowLoad;
    portfolioTabSlider.edgtfOnWindowResize = edgtfOnWindowResize;
    portfolioTabSlider.edgtfOnWindowScroll = edgtfOnWindowScroll;
    portfolioTabSlider.edgtfInitPortfolioTabsSlider = edgtfInitPortfolioTabsSlider;

    $(document).ready(edgtfOnDocumentReady);
    $(window).load(edgtfOnWindowLoad);
    $(window).resize(edgtfOnWindowResize);
    $(window).scroll(edgtfOnWindowScroll);

    /*
     All functions to be called on $(document).ready() should be in this function
     */
    function edgtfOnDocumentReady() {
        edgtfInitPortfolioTabsSlider();
    }

    /*
     All functions to be called on $(window).load() should be in this function
     */
    function edgtfOnWindowLoad() {

    }

    /*
     All functions to be called on $(window).resize() should be in this function
     */
    function edgtfOnWindowResize() {
    }

    /*
     All functions to be called on $(window).scroll() should be in this function
     */
    function edgtfOnWindowScroll() {
    }

    /**
     * Initializes portfolio tabs slider
     */
    function edgtfInitPortfolioTabsSlider(){
        var portSlider = $('.edgtf-portfolio-tabs-slider');

        // there is part of code in common.js which block regular function for owl slider to execute
        // see @edgtfOwlSlider

        // if (slider.hasClass('edgtf-pts-content')) {
        //     return; // break this function for portfolio tabs slider
        // }

        if(portSlider.length){
            portSlider.each(function(){
                var thisPortSlider = $(this),
                    portSliderMenu = thisPortSlider.find('.edgtf-pts-menu'),
                    portSliderMenuItem = portSliderMenu.find('li'),
                    portSliderMenuLink = portSliderMenu.find('a'),
                    portSliderContent = thisPortSlider.find('.edgtf-pts-content'),
                    margin;

                margin = edgtf.windowWidth / 10;

                portSliderContent.owlCarousel({
                    items: 1,
                    margin: margin,
                    loop: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    smartSpeed: 1200,
                    dots: true,
                    responsive:{
                        0:{
                            autoplayTimeout: 8000,
                            margin: 30
                        },
                        769:{
                            autoplayTimeout: 5000,
                            margin: 70
                        },
                        1400:{
                            autoplayTimeout: 5000,
                            margin: margin
                        }
                    },
                    onInitialized: function() {
                        portSliderContent.css('visibility', 'visible');
                        portSliderMenuItem.eq(0).children('a').addClass('edgtf-active');
                    }
                });

                portSliderContent.on('changed.owl.carousel', function(event) {
                    var currentItemIndex  = event.page.index,
                        numberOfItems = portSliderContent.find('.owl-item').length,
                        menuIndex = currentItemIndex; // 2 is number of cloned items per side

                    if(currentItemIndex === numberOfItems) { // 2 is number of cloned items per side
                        menuIndex = 0;
                    }

                    portSliderMenuLink.removeClass('edgtf-active');

                    portSliderMenuItem.eq(menuIndex).children('a').addClass('edgtf-active');
                });

                portSliderMenuLink.on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();

                    var thisItem = $(this),
                        thisItemIndex = thisItem.parent().index();

                    portSliderContent.trigger('to.owl.carousel', [thisItemIndex, 1200, true]);
                });
            });
        }
    }
    
})(jQuery);