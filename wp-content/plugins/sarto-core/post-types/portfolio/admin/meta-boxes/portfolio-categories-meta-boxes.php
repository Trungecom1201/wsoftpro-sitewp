<?php

if ( ! function_exists( 'sarto_edge_portfolio_category_additional_fields' ) ) {
	function sarto_edge_portfolio_category_additional_fields() {
		
		$fields = sarto_edge_add_taxonomy_fields(
			array(
				'scope' => 'portfolio-category',
				'name'  => 'portfolio_category_options'
			)
		);
		
		sarto_edge_add_taxonomy_field(
			array(
				'name'   => 'edgtf_portfolio_category_image_meta',
				'type'   => 'image',
				'label'  => esc_html__( 'Category Image', 'sarto-core' ),
				'parent' => $fields
			)
		);
	}
	
	add_action( 'sarto_edge_custom_taxonomy_fields', 'sarto_edge_portfolio_category_additional_fields' );
}