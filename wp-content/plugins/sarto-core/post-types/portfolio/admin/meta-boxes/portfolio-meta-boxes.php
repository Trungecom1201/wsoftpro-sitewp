<?php

if ( ! function_exists( 'sarto_core_map_portfolio_meta' ) ) {
	function sarto_core_map_portfolio_meta() {
		global $sarto_Framework;
		
		$sarto_pages = array();
		$pages      = get_pages();
		foreach ( $pages as $page ) {
			$sarto_pages[ $page->ID ] = $page->post_title;
		}
		
		//Portfolio Images
		
		$sarto_portfolio_images = new SartoEdgeMetaBox( 'portfolio-item', esc_html__( 'Portfolio Images (multiple upload)', 'sarto-core' ), '', '', 'portfolio_images' );
		$sarto_Framework->edgtMetaBoxes->addMetaBox( 'portfolio_images', $sarto_portfolio_images );
		
		$sarto_portfolio_image_gallery = new SartoEdgeMultipleImages( 'edgtf-portfolio-image-gallery', esc_html__( 'Portfolio Images', 'sarto-core' ), esc_html__( 'Choose your portfolio images', 'sarto-core' ) );
		$sarto_portfolio_images->addChild( 'edgtf-portfolio-image-gallery', $sarto_portfolio_image_gallery );
		
		//Portfolio Single Upload Images/Videos 
		
		$sarto_portfolio_images_videos = sarto_edge_create_meta_box(
			array(
				'scope' => array( 'portfolio-item' ),
				'title' => esc_html__( 'Portfolio Images/Videos (single upload)', 'sarto-core' ),
				'name'  => 'edgtf_portfolio_images_videos'
			)
		);
		sarto_edge_add_repeater_field(
			array(
				'name'        => 'edgtf_portfolio_single_upload',
				'parent'      => $sarto_portfolio_images_videos,
				'button_text' => esc_html__( 'Add Image/Video', 'sarto-core' ),
				'fields'      => array(
					array(
						'type'        => 'select',
						'name'        => 'file_type',
						'label'       => esc_html__( 'File Type', 'sarto-core' ),
						'options' => array(
							'image' => esc_html__('Image','sarto-core'),
							'video' => esc_html__('Video','sarto-core'),
						)
					),
					array(
						'type'        => 'image',
						'name'        => 'single_image',
						'label'       => esc_html__( 'Image', 'sarto-core' ),
						'dependency' => array(
							'show' => array(
								'file_type'  => 'image'
							)
						)
					),
					array(
						'type'        => 'select',
						'name'        => 'video_type',
						'label'       => esc_html__( 'Video Type', 'sarto-core' ),
						'options'	  => array(
							'youtube' => esc_html__('YouTube', 'sarto-core'),
							'vimeo' => esc_html__('Vimeo', 'sarto-core'),
							'self' => esc_html__('Self Hosted', 'sarto-core'),
						),
						'dependency' => array(
							'show' => array(
								'file_type'  => 'video'
							)
						)
					),
					array(
						'type'        => 'text',
						'name'        => 'video_id',
						'label'       => esc_html__( 'Video ID', 'sarto-core' ),
						'dependency' => array(
							'show' => array(
								'file_type' => 'video',
								'video_type'  => array('youtube','vimeo')
							)
						)
					),
					array(
						'type'        => 'text',
						'name'        => 'video_mp4',
						'label'       => esc_html__( 'Video mp4', 'sarto-core' ),
						'dependency' => array(
							'show' => array(
								'file_type' => 'video',
								'video_type'  => 'self'
							)
						)
					),
					array(
						'type'        => 'image',
						'name'        => 'video_cover_image',
						'label'       => esc_html__( 'Video Cover Image', 'sarto-core' ),
						'dependency' => array(
							'show' => array(
								'file_type' => 'video',
								'video_type'  => 'self'
							)
						)
					)
				)
			)
		);
		
		//Portfolio Additional Sidebar Items
		
		$sarto_additional_sidebar_items = sarto_edge_create_meta_box(
			array(
				'scope' => array( 'portfolio-item' ),
				'title' => esc_html__( 'Additional Portfolio Sidebar Items', 'sarto-core' ),
				'name'  => 'portfolio_properties'
			)
		);

		sarto_edge_add_repeater_field(
			array(
				'name'        => 'edgtf_portfolio_properties',
				'parent'      => $sarto_additional_sidebar_items,
				'button_text' => esc_html__( 'Add New Item', 'sarto-core' ),
				'fields'      => array(
					array(
						'type'        => 'text',
						'name'        => 'item_title',
						'label'       => esc_html__( 'Item Title', 'sarto-core' ),
					),
					array(
						'type'        => 'text',
						'name'        => 'item_text',
						'label'       => esc_html__( 'Item Text', 'sarto-core' )
					),
					array(
						'type'        => 'text',
						'name'        => 'item_url',
						'label'       => esc_html__( 'Enter Full URL for Item Text Link', 'sarto-core' )
					)
				)
			)
		);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_core_map_portfolio_meta', 40 );
}