<?php

namespace SartoCore\CPT\Shortcodes\SectionHolder;

use SartoCore\Lib;

class SectionItem implements Lib\ShortcodeInterface {
	private $base;

	function __construct() {
		$this->base = 'edgtf_section_item';
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map( array(
					'name'            => esc_html__( 'Section Item', 'sarto-core' ),
					'base'            => $this->base,
					'as_child'        => array( 'only' => 'edgtf_section_holder' ),
					'as_parent'       => array( 'except' => 'vc_row, vc_accordion, edgtf_portfolio_list, edgtf_portfolio_slider' ),
					'content_element' => true,
					'category'        => esc_html__( 'by SARTO', 'sarto-core' ),
					'icon'            => 'icon-wpb-section-item extended-custom-icon',
					'js_view'         => 'VcColumnView',
					'params'          => array_merge( array(
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Background Color', 'sarto-core' ),
							'param_name'  => 'background_color',
							'value'       => '',
							'description' => ''
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'disable_padding_on_item',
							'heading'     => esc_html__('Disable Item Padding', 'sarto-core'),
							'value'       => array_flip(sarto_edge_get_yes_no_select_array(true)),
						),
						array(
							'type'       => 'dropdown',
							'heading'    => esc_html__( 'Horizontal Alignment', 'sarto-core' ),
							'param_name' => 'horizontal_aligment',
							'value'      => array(
								esc_html__( 'Left', 'sarto-core' )   => 'left',
								esc_html__( 'Right', 'sarto-core' )  => 'right',
								esc_html__( 'Center', 'sarto-core' ) => 'center'
							),
						),
						array(
							'type'       => 'dropdown',
							'heading'    => esc_html__( 'Vertical Alignment', 'sarto-core' ),
							'param_name' => 'vertical_alignment',
							'value'      => array(
								esc_html__( 'Middle', 'sarto-core' ) => 'middle',
								esc_html__( 'Top', 'sarto-core' )    => 'top',
								esc_html__( 'Bottom', 'sarto-core' ) => 'bottom'
							),
						),
		            	array(
		            	    'type'        => 'attach_image',
		            	    'param_name'  => 'hover_image',
		            	    'heading'     => esc_html__( 'Hover Image', 'sarto-core' ),
							'description'  => esc_html__( 'Please note that after uploading a hover image, Icon usage gets disabled.', 'sarto-core' ),
		            	)
					), sarto_edge_icon_collections()->getVCParamsArray(array(), '', true),
					array(
						array(
							'type'       => 'attach_image',
							'param_name' => 'custom_icon',
							'heading'    => esc_html__( 'Custom Icon', 'sarto-core' )
						),
						array(
							'type'        => 'colorpicker',
							'heading'     => esc_html__( 'Icon Holder Background Color', 'sarto-core' ),
							'param_name'  => 'icon_holder_background_color',
							'value'       => '',
							'description' => '',
							'dependency'  => array(
								'element'   => 'icon_pack',
								'not_empty' => true
							),
						),
					)
				)
			) );
		}
	}

	public function render( $atts, $content = null ) {
		$args = array(
			'background_color'             => '',
			'disable_padding_on_item'     => '',
			'horizontal_aligment'          => 'left',
			'vertical_alignment'           => 'middle',
			'custom_icon'                  => '',
			'icon_holder_background_color' => '',
			'hover_image'				   => ''
		);

		$args   = array_merge( $args, sarto_edge_icon_collections()->getShortcodeParams() );
		$params = shortcode_atts( $args, $atts );

		$params['content'] = $content;
		$params['icon_parameters'] = $this->getIconParameters( $params );

		$params['section_item_style'] = $this->getSectionItemStyle( $params );
		$params['section_item_overlay_style'] = $this->getSectionItemOverlayStyle( $params );
		$params['section_item_class'] = $this->getSectionItemClass( $params );

		$html = sarto_core_get_shortcode_module_template_part( 'templates/section-item-template', 'section-holder', '', $params );

		return $html;
	}


	/**
	 * Return Section Item style
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getSectionItemStyle( $params ) {

		$section_item_style = array();

		if ( $params['background_color'] !== '' ) {
			$section_item_style[] = 'background-color: ' . $params['background_color'];
		}

		return implode( ';', $section_item_style );

	}

	/**
	 * Return Section Item Overlay style
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getSectionItemOverlayStyle( $params ) {

		$section_item_overlay_style = array();

		if ( $params['background_color'] !== '' ) {
			$section_item_overlay_style[] = 'background-color: ' . $params['icon_holder_background_color'];
		}

		return implode( ';', $section_item_overlay_style );

	}


	/**
	 * Return Section Item classes
	 *
	 * @param $params
	 *
	 * @return array
	 */
	private function getSectionItemClass( $params ) {

		$section_item_class = array();

		if ( $params['vertical_alignment'] !== '' ) {
			$section_item_class[] = 'edgtf-vertical-alignment-' . $params['vertical_alignment'];
		}

		if ( $params['horizontal_aligment'] !== '' ) {
			$section_item_class[] = 'edgtf-horizontal-alignment-' . $params['horizontal_aligment'];
		}

		if (( $params['custom_icon'] !== '' || $params['icon_pack'] !== '' ) && empty( $params['hover_image'] )) {
			$section_item_class[] = 'edgtf-section-item-icon-overlay';
		}

		if (!empty( $params['hover_image'] )) {
			$section_item_class[] = 'edgtf-si-with-hover-image';
		}

		if ( $params['disable_padding_on_item'] == 'yes') {
			$section_item_class[] = 'edgtf-section-item-disable-padding';
		}

		return implode( ' ', $section_item_class );

	}

	private function getIconParameters( $params ) {
		$params_array = array();

		if ( empty( $params['custom_icon']) &&  !empty( $params['icon_pack'] ) ) {
			$iconPackName = sarto_edge_icon_collections()->getIconCollectionParamNameByKey( $params['icon_pack'] );

			$params_array['icon_pack']     = $params['icon_pack'];
			$params_array[ $iconPackName ] = $params[ $iconPackName ];
		}

		return $params_array;
	}


}
