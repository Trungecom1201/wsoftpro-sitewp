<div <?php sarto_edge_class_attribute($section_classes)?>>
	<div class="edgtf-sh-title-area" <?php sarto_edge_inline_style($title_area_style);?>>
		<div class="edgtf-sh-title-area-inner">
			<?php 
			if ($title !== '' || $subtitle !== '') {
				echo sarto_edge_execute_shortcode('edgtf_section_title', $title_params);
			}
			?>
		</div>
		<?php if ($uncovering_animation == 'yes') : ?>
			<div class="edgtf-sh-mask edgtf-sh-mask-intro"  <?php echo sarto_edge_get_inline_style( $mask_styles['first'] ); ?>></div>
			<div class="edgtf-sh-mask edgtf-sh-mask-default" <?php echo sarto_edge_get_inline_style( $mask_styles['second'] ); ?>></div>
		<?php endif; ?>
	</div>
	<div class="edgtf-sh-content-area">
		<?php echo do_shortcode($content);?>
	</div>
</div>