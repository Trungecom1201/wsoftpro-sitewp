<?php
if ( ! function_exists( 'sarto_core_add_content_switch_intro' ) ) {
	function sarto_core_add_content_switch_intro( $shortcodes_class_name ) {
		$shortcodes = array(
			'SartoCore\CPT\Shortcodes\ContentSwitchIntro\ContentSwitchIntro'
		);
		
		$shortcodes_class_name = array_merge( $shortcodes_class_name, $shortcodes );
		
		return $shortcodes_class_name;
	}
	
	add_filter( 'sarto_core_filter_add_vc_shortcode', 'sarto_core_add_content_switch_intro' );
}

if ( ! function_exists( 'sarto_core_set_content_switch_intro_icon_class_name_for_vc_shortcodes' ) ) {
	/**
	 * Function that set custom icon class name for content switch intro shortcode to set our icon for Visual Composer shortcodes panel
	 */
	function sarto_core_set_content_switch_intro_icon_class_name_for_vc_shortcodes( $shortcodes_icon_class_array ) {
		$shortcodes_icon_class_array[] = '.icon-wpb-content-switch-intro';
		
		return $shortcodes_icon_class_array;
	}
	
	add_filter( 'sarto_core_filter_add_vc_shortcodes_custom_icon_class', 'sarto_core_set_content_switch_intro_icon_class_name_for_vc_shortcodes' );
}

if (sarto_core_theme_installed() ) {
	if (!function_exists('sarto_edge_content_switch_intro_body_class')) {
		/**
		 * Function that adds body classes if content switch shortcode present
		 *
		 * @param $classes array original array of body classes
		 *
		 * @return array modified array of classes
		 */
		function sarto_edge_content_switch_intro_body_class($classes)
		{
			if (sarto_edge_has_shortcode('edgtf_content_switch_intro')) {
				$classes[] = 'edgtf-content-switch-intro-on-page';
			}
			return $classes;
		}

		add_filter('body_class', 'sarto_edge_content_switch_intro_body_class');
	}
}