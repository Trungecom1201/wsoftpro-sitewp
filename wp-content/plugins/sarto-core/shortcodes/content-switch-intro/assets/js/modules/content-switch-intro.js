(function ($) {
	'use strict';
	
	var contentSwitchIntro = {};
	edgtf.modules.contentSwitchIntro = contentSwitchIntro;
	
	contentSwitchIntro.edgtfContentSwitchIntro = edgtfContentSwitchIntro;
	
	contentSwitchIntro.edgtfOnDocumentReady = edgtfOnDocumentReady;
	
	$(document).ready(edgtfOnDocumentReady);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function edgtfOnDocumentReady() {
		edgtfContentSwitchIntro();
	}

	function edgtfContentSwitchIntro() {
		var csi = $('#edgtf-content-switch-intro');

		if (csi.length) {
			var prevNav = csi.find('.edgtf-csi-prev'),
				nextNav = csi.find('.edgtf-csi-next'),
				numOfOriginals = csi.find('.edgtf-csi-item').length,
				staticLayer = csi.find('.edgtf-csi-static-layer').length,
				transitionEffect = staticLayer ? 'fade' : 'slide',
				readyToScroll = false,
				prevIndex,
				nextIndex;

			var setNavState = function(activeIndex) {
				switch (activeIndex) {
					default: 
						prevIndex = activeIndex - 1;
						nextIndex = activeIndex + 1;
						break;
					case 1:
						prevIndex = numOfOriginals;
						nextIndex = 2;
						break;
					case numOfOriginals :
						prevIndex = numOfOriginals - 1;
						nextIndex = 1;
						break;
				}

				prevNav.text(prevIndex);
				nextNav.text(nextIndex);
			}

			var csiSwiper = new Swiper(csi.find('.swiper-container'), {
				loop: true,
			    speed: 800,
			    autoplay: {
			   	 	delay: 3500,
			   	 	disableOnInteraction: false
			    },
			    navigation: {
			        nextEl: csi.find('.edgtf-csi-next'),
			        prevEl: csi.find('.edgtf-csi-prev'),
			    },
			    parallax: true,
				effect: transitionEffect,
				fade: {
					crossfade: true,
				},
			    init: false
			});

			csiSwiper.on('init', function () {
			    csi.addClass('edgtf-csi-swiper-start');
			});

			csiSwiper.on('transitionStart', function () {
			    setNavState(csi.find('.swiper-slide-active').data('index'));
			});

			var csiStart = function() {
				csi.addClass('edgtf-animate');

				if (csi.find('.edgtf-csi-item').length > 1) {
					csi.find('.edgtf-csi-mask-default').one(edgtf.animationEnd, function() {
						csiSwiper.init();
					});
				} else {
					csi.addClass('edgtf-csi-single-item');
				}

				csi.find('.edgtf-csi-mask-aux').one(edgtf.animationEnd, function() {
					readyToScroll = true;
					$(document).trigger('edgtfCSILoaded');
	                edgtf.modules.common.edgtfInitParallax();
				});
			}

			
			//prevent mousewheel scroll
			window.addEventListener('wheel', function (event) {
			    if (!readyToScroll) {
			        event.preventDefault();
			    }
			});

			$(window).on('load', function() {
			    $('html, body').animate({scrollTop: 0}, 100, function() {
			    	edgtf.body.addClass('edgtf-content-switch-intro-loaded');
			    	csi.addClass('edgtf-remove-cover').one(edgtf.transitionEnd, function() {
			    		$('header').css('opacity', '1');
			    		csiStart();
			    		csi.find('.edgtf-csi-cover').remove();
			    	});
			    });
			});
		}	
	}
	
})(jQuery);