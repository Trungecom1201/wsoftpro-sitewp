<?php
namespace SartoCore\CPT\Shortcodes\ClientsHolder;

use SartoCore\Lib;

class ClientsHolder implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_clients_holder';
		add_action('vc_before_init', array($this, 'vcMap'));
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if(function_exists('vc_map')) {
			vc_map(
				array(
					'name'      => esc_html__( 'Clients Grid', 'sarto-core' ),
					'base'      => $this->base,
					'icon'      => 'icon-wpb-clients-wc-holder extended-custom-icon',
					'category'  => esc_html__( 'by SARTO', 'sarto-core' ),
					'as_parent' => array( 'only' => 'edgtf_clients_carousel_item' ),
					'js_view'   => 'VcColumnView',
					'params'    => array(
						array(
							'type'        => 'dropdown',
							'param_name'  => 'number_of_columns',
							'heading'     => esc_html__( 'Number of Columns', 'sarto-core' ),
							'value'       => array(
								esc_html__( 'One', 'sarto-core' )   => '1',
								esc_html__( 'Two', 'sarto-core' )   => '2',
								esc_html__( 'Three', 'sarto-core' ) => '3',
								esc_html__( 'Four', 'sarto-core' )  => '4',
								esc_html__( 'Five', 'sarto-core' )  => '5',
								esc_html__( 'Six', 'sarto-core' )   => '6'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'space_between_columns',
							'heading'     => esc_html__( 'Space Between Columns', 'sarto-core' ),
							'value'       => array(
								esc_html__( 'Normal', 'sarto-core' )   => 'normal',
								esc_html__( 'Small', 'sarto-core' )    => 'small',
								esc_html__( 'Tiny', 'sarto-core' )     => 'tiny',
								esc_html__( 'No Space', 'sarto-core' ) => 'no'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'image_alignment',
							'heading'     => esc_html__( 'Item Horizontal Alignment', 'sarto-core' ),
							'value'       => array(
								esc_html__( 'Default', 'sarto-core' ) => '',
								esc_html__( 'Left', 'sarto-core' )    => 'left',
								esc_html__( 'Center', 'sarto-core' )  => 'center',
								esc_html__( 'Right', 'sarto-core' )   => 'right'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'items_hover_animation',
							'heading'     => esc_html__( 'Items Hover Animation', 'sarto-core' ),
							'value'       => array(
								esc_html__( 'Switch Images', 'sarto-core' ) => 'switch-images',
								esc_html__( 'Roll Over', 'sarto-core' )     => 'roll-over',
								esc_html__( 'Show Shadow', 'sarto-core' )   => 'shadow',
								esc_html__( 'Zoom Image', 'sarto-core' )    => 'zoom'
							),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'borders',
							'heading'     => esc_html__( 'Enable Borders Around Elements', 'sarto-core' ),
							'value'       => array_flip(sarto_edge_get_yes_no_select_array(false, false)),
							'save_always' => true
						)
					)
				)
			);
		}
	}

	public function render($atts, $content = null) {
		$args = array(
			'number_of_columns' 	=> '3',
			'space_between_columns'	=> 'normal',
			'image_alignment' 	    => '',
			'items_hover_animation' => 'switch-images',
			'borders'               => 'no'
		);
		$params = shortcode_atts($args, $atts);
		
		$params['holder_classes'] = $this->getHolderClasses($params, $args);
		$params['content'] = $content;

		$html = sarto_core_get_shortcode_module_template_part( 'templates/clients-grid', 'clients-grid', '', $params );

		return $html;
	}

	/**
	 * Generates holder classes
	 *
	 * @param $params
	 *
	 * @return string
	 */
	private function getHolderClasses($params, $args) {
		$holderClasses = '';
		
		$holderClasses .= !empty($params['number_of_columns']) ? ' edgtf-ch-columns-' . $params['number_of_columns'] : ' edgtf-ch-columns-' . $args['number_of_columns'];
		$holderClasses .= !empty($params['space_between_columns']) ? ' edgtf-ch-' . $params['space_between_columns'] . '-space' : ' edgtf-ch-' . $args['space_between_items'] . '-space';
		$holderClasses .= !empty($params['image_alignment']) ? ' edgtf-ch-alignment-' . $params['image_alignment'] : '';
		$holderClasses .= !empty($params['items_hover_animation']) ? ' edgtf-cc-hover-'.$params['items_hover_animation'] : ' edgtf-cc-hover-switch-images';
		$holderClasses .= $params['borders'] === 'yes' ? ' edgtf-cc-borders' : '';
		
		return $holderClasses;
	}
}
