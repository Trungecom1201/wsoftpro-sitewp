<?php

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
	class WPBakeryShortCode_Edgtf_Clients_Holder extends WPBakeryShortCodesContainer {}
}

if(!function_exists('sarto_core_add_clients_holder_shortcodes')) {
	function sarto_core_add_clients_holder_shortcodes($shortcodes_class_name) {
		$shortcodes = array(
			'SartoCore\CPT\Shortcodes\ClientsHolder\ClientsHolder'
		);
		
		$shortcodes_class_name = array_merge($shortcodes_class_name, $shortcodes);
		
		return $shortcodes_class_name;
	}
	
	add_filter('sarto_core_filter_add_vc_shortcode', 'sarto_core_add_clients_holder_shortcodes');
}

if( !function_exists('sarto_core_set_clients_holder_icon_class_name_for_vc_shortcodes') ) {
	/**
	 * Function that set custom icon class name for clients holder shortcode to set our icon for Visual Composer shortcodes panel
	 */
	function sarto_core_set_clients_holder_icon_class_name_for_vc_shortcodes($shortcodes_icon_class_array) {
		$shortcodes_icon_class_array[] = '.icon-wpb-clients-holder-wc-holder';
		
		return $shortcodes_icon_class_array;
	}
	
	add_filter('sarto_core_filter_add_vc_shortcodes_custom_icon_class', 'sarto_core_set_clients_holder_icon_class_name_for_vc_shortcodes');
}