<<?php echo esc_attr($title_tag); ?> class="edgtf-accordion-title">
	<span class="edgtf-tab-title"><?php echo esc_html($title); ?></span>
    <span class="edgtf-accordion-mark">
		<span class="edgtf-accordion-mark-icon">
			<span class="icon_plus"></span>
			<span class="icon_close"></span>
		</span>
	</span>
</<?php echo esc_attr($title_tag); ?>>
<div class="edgtf-accordion-content">
	<div class="edgtf-accordion-content-inner">
		<?php echo do_shortcode($content); ?>
	</div>
</div>