<?php
namespace SartoCore\CPT\Shortcodes\ElementsHolder;

use SartoCore\Lib;

class ElementsHolderItem implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_elements_holder_item';
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'                    => esc_html__( 'Elements Holder Item', 'sarto-core' ),
					'base'                    => $this->base,
					'as_child'                => array( 'only' => 'edgtf_elements_holder' ),
					'as_parent'               => array( 'except' => 'vc_row, vc_accordion' ),
					'content_element'         => true,
					'category'                => esc_html__( 'by SARTO', 'sarto-core' ),
					'icon'                    => 'icon-wpb-elements-holder-item extended-custom-icon',
					'show_settings_on_create' => true,
					'js_view'                 => 'VcColumnView',
					'params'                  => array(
						array(
							'type'        => 'textfield',
							'param_name'  => 'custom_class',
							'heading'     => esc_html__( 'Custom CSS Class', 'sarto-core' ),
							'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS', 'sarto-core' )
						),
						array(
							'type'       => 'colorpicker',
							'param_name' => 'background_color',
							'heading'    => esc_html__( 'Background Color', 'sarto-core' )
						),
						array(
							'type'       => 'attach_image',
							'param_name' => 'background_image',
							'heading'    => esc_html__( 'Background Image', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'item_padding',
							'heading'     => esc_html__( 'Padding', 'sarto-core' ),
							'description' => esc_html__( 'Please insert padding in format 0px 10px 0px 10px', 'sarto-core' )
						),
						array(
							'type'       => 'dropdown',
							'param_name' => 'horizontal_alignment',
							'heading'    => esc_html__( 'Horizontal Alignment', 'sarto-core' ),
							'value'      => array(
								esc_html__( 'Left', 'sarto-core' )   => 'left',
								esc_html__( 'Right', 'sarto-core' )  => 'right',
								esc_html__( 'Center', 'sarto-core' ) => 'center'
							)
						),
						array(
							'type'       => 'dropdown',
							'param_name' => 'vertical_alignment',
							'heading'    => esc_html__( 'Vertical Alignment', 'sarto-core' ),
							'value'      => array(
								esc_html__( 'Middle', 'sarto-core' ) => 'middle',
								esc_html__( 'Top', 'sarto-core' )    => 'top',
								esc_html__( 'Bottom', 'sarto-core' ) => 'bottom'
							)
						),
						array(
							'type'       => 'dropdown',
							'param_name' => 'behavior',
							'heading'    => esc_html__( 'Behavior', 'sarto-core' ),
							'value'      => array(
								esc_html__( 'Default', 'sarto-core' ) => '',
								esc_html__( 'Hover Image', 'sarto-core' ) => 'hover-image',
								esc_html__( 'Uncover Animation', 'sarto-core' ) => 'uncover-animation'
							),
							'save_always' => true
						),
                    	array(
                    	    'type'        => 'attach_image',
                    	    'param_name'  => 'hover_image',
                    	    'heading'     => esc_html__( 'Hover Image', 'sarto-core' ),
							'dependency'  => array( 'element' => 'behavior', 'value' => array( 'hover-image' ) )
                    	),
                    	array(
                    		'type'       => 'colorpicker',
                    		'param_name' => 'first_mask_color',
                    		'heading'    => esc_html__( 'First Mask Color', 'sarto-core' ),
							'dependency'  => array( 'element' => 'behavior', 'value' => array( 'uncover-animation' ) )
                    	),
                    	array(
                    		'type'       => 'colorpicker',
                    		'param_name' => 'second_mask_color',
                    		'heading'    => esc_html__( 'Second Mask Color', 'sarto-core' ),
							'dependency'  => array( 'element' => 'behavior', 'value' => array( 'uncover-animation' ) )
                    	),
						array(
							'type'       => 'dropdown',
							'param_name' => 'animation',
							'heading'    => esc_html__( 'Animation Type', 'sarto-core' ),
							'value'      => array(
								esc_html__( 'Default (No Animation)', 'sarto-core' )   => '',
								esc_html__( 'Element Grow In', 'sarto-core' )          => 'edgtf-grow-in',
								esc_html__( 'Element Fade In Down', 'sarto-core' )     => 'edgtf-fade-in-down',
								esc_html__( 'Element From Fade', 'sarto-core' )        => 'edgtf-element-from-fade',
								esc_html__( 'Element From Left', 'sarto-core' )        => 'edgtf-element-from-left',
								esc_html__( 'Element From Right', 'sarto-core' )       => 'edgtf-element-from-right',
								esc_html__( 'Element From Top', 'sarto-core' )         => 'edgtf-element-from-top',
								esc_html__( 'Element From Bottom', 'sarto-core' )      => 'edgtf-element-from-bottom',
								esc_html__( 'Element Flip In', 'sarto-core' )          => 'edgtf-flip-in',
								esc_html__( 'Element X Rotate', 'sarto-core' )         => 'edgtf-x-rotate',
								esc_html__( 'Element Z Rotate', 'sarto-core' )         => 'edgtf-z-rotate',
								esc_html__( 'Element Y Translate', 'sarto-core' )      => 'edgtf-y-translate',
								esc_html__( 'Element Fade In X Rotate', 'sarto-core' ) => 'edgtf-fade-in-left-x-rotate',
							),
							'dependency'  => array( 'element' => 'behavior', 'not_empty' => false )
						),
						array(
							'type'       => 'textfield',
							'param_name' => 'animation_delay',
							'heading'    => esc_html__( 'Animation Delay (ms)', 'sarto-core' ),
							'dependency'  => array( 'element' => 'animation', 'not_empty' => true )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'item_padding_1281_1600',
							'heading'     => esc_html__( 'Padding on screen size between 1281px-1600px', 'sarto-core' ),
							'description' => esc_html__( 'Please insert padding in format top right bottom left. For example 10px 0 10px 0', 'sarto-core' ),
							'group'       => esc_html__( 'Width & Responsiveness', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'item_padding_1025_1280',
							'heading'     => esc_html__( 'Padding on screen size between 1025px-1280px', 'sarto-core' ),
							'description' => esc_html__( 'Please insert padding in format top right bottom left. For example 10px 0 10px 0', 'sarto-core' ),
							'group'       => esc_html__( 'Width & Responsiveness', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'item_padding_769_1024',
							'heading'     => esc_html__( 'Padding on screen size between 769px-1024px', 'sarto-core' ),
							'description' => esc_html__( 'Please insert padding in format 0px 10px 0px 10px', 'sarto-core' ),
							'group'       => esc_html__( 'Width & Responsiveness', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'item_padding_681_768',
							'heading'     => esc_html__( 'Padding on screen size between 681px-768px', 'sarto-core' ),
							'description' => esc_html__( 'Please insert padding in format 0px 10px 0px 10px', 'sarto-core' ),
							'group'       => esc_html__( 'Width & Responsiveness', 'sarto-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'item_padding_680',
							'heading'     => esc_html__( 'Padding on screen size bellow 680px', 'sarto-core' ),
							'description' => esc_html__( 'Please insert padding in format 0px 10px 0px 10px', 'sarto-core' ),
							'group'       => esc_html__( 'Width & Responsiveness', 'sarto-core' )
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'custom_class'           => '',
			'background_color'       => '',
			'background_image'       => '',
			'item_padding'           => '',
			'horizontal_alignment'   => '',
			'vertical_alignment'     => '',
			'behavior'				 => '',
			'hover_image'			 => '',
			'first_mask_color'		 => '',
			'second_mask_color'		 => '',
			'animation'              => '',
			'animation_delay'        => '',
			'item_padding_1281_1600' => '',
			'item_padding_1025_1280' => '',
			'item_padding_769_1024'  => '',
			'item_padding_681_768'   => '',
			'item_padding_680'       => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['content']           = $content;
		$params['holder_classes']    = $this->getHolderClasses( $params );
		$params['holder_rand_class'] = 'edgtf-eh-custom-' . mt_rand( 1000, 10000 );
		$params['holder_styles']     = $this->getHolderStyles( $params );
		$params['content_styles']    = $this->getContentStyles( $params );
		$params['holder_data']       = $this->getHolderData( $params );
		$params['mask_styles'] 		 = $this->getMaskStyles( $params );

		$html = sarto_core_get_shortcode_module_template_part( 'templates/elements-holder-item-template', 'elements-holder', '', $params );
		
		return $html;
	}
	
	private function getHolderClasses( $params ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['custom_class'] ) ? esc_attr( $params['custom_class'] ) : '';
		$holderClasses[] = ! empty( $params['vertical_alignment'] ) ? 'edgtf-vertical-alignment-' . $params['vertical_alignment'] : '';
		$holderClasses[] = ! empty( $params['horizontal_alignment'] ) ? 'edgtf-horizontal-alignment-' . $params['horizontal_alignment'] : '';
		$holderClasses[] = ! empty( $params['animation'] ) ? $params['animation'] : '';
		$holderClasses[] = ! empty( $params['behavior'] ) ? 'edgtf-ehi-with-' . $params['behavior'] : '';
		
		return implode( ' ', $holderClasses );
	}
	
	private function getHolderStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['background_color'] ) ) {
			$styles[] = 'background-color: ' . $params['background_color'];
		}
		
		if ( ! empty( $params['background_image'] ) ) {
			$styles[] = 'background-image: url(' . wp_get_attachment_url( $params['background_image'] ) . ')';
		}
		
		return implode( ';', $styles );
	}
	
	private function getContentStyles( $params ) {
		$styles = array();
		
		if ( $params['item_padding'] !== '' ) {
			$styles[] = 'padding: ' . $params['item_padding'];
		}
		
		return implode( ';', $styles );
	}
	
	private function getHolderData( $params ) {
		$data                    = array();
		$data['data-item-class'] = $params['holder_rand_class'];
		
		if ( ! empty( $params['animation'] ) ) {
			$data['data-animation'] = $params['animation'];
		}
		
		if ( $params['animation_delay'] !== '' ) {
			$data['data-animation-delay'] = esc_attr( $params['animation_delay'] );
		}
		
		if ( $params['item_padding_1281_1600'] !== '' ) {
			$data['data-1281-1600'] = $params['item_padding_1281_1600'];
		}
		
		if ( $params['item_padding_1025_1280'] !== '' ) {
			$data['data-1025-1280'] = $params['item_padding_1025_1280'];
		}
		
		if ( $params['item_padding_769_1024'] !== '' ) {
			$data['data-769-1024'] = $params['item_padding_769_1024'];
		}
		
		if ( $params['item_padding_681_768'] !== '' ) {
			$data['data-681-768'] = $params['item_padding_681_768'];
		}
		
		if ( $params['item_padding_680'] !== '' ) {
			$data['data-680'] = $params['item_padding_680'];
		}
		
		return $data;
	}

	private function getMaskStyles( $params ) {
		$styles = array();
		$styles['first'] = '';
		$styles['second'] = '';
		
		if ( ! empty( $params['first_mask_color'] ) ) {
			$styles['first'] = 'background-color: ' . $params['first_mask_color'];
		}

		if ( ! empty( $params['second_mask_color'] ) ) {
			$styles['second'] = 'background-color: ' . $params['second_mask_color'];
		}
		
		return $styles;
	}
}
