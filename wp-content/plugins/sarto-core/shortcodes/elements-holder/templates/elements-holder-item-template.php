<div class="edgtf-eh-item <?php echo esc_attr($holder_classes); ?>" <?php echo sarto_edge_get_inline_style($holder_styles); ?> <?php echo sarto_edge_get_inline_attrs($holder_data); ?>>
	<div class="edgtf-eh-item-inner">
		<div class="edgtf-eh-item-content <?php echo esc_attr($holder_rand_class); ?>" <?php echo sarto_edge_get_inline_style($content_styles); ?>>
			<?php echo do_shortcode($content); ?>
		</div>
	</div>
	<?php if ($behavior == 'hover-image') : 
		$hover_style = "background-image: url(" . wp_get_attachment_url( $hover_image ) . ");"; 
	?>
		<div class="edgtf-eh-item-hover-image" <?php echo sarto_edge_get_inline_style( $hover_style ); ?>></div>
	<?php endif; ?>
	<?php if ($behavior == 'uncover-animation') : ?>
		<div class="edgtf-ehi-mask edgtf-ehi-mask-intro"  <?php echo sarto_edge_get_inline_style( $mask_styles['first'] ); ?>></div>
		<div class="edgtf-ehi-mask edgtf-ehi-mask-default" <?php echo sarto_edge_get_inline_style( $mask_styles['second'] ); ?>></div>
	<?php endif; ?>
</div>