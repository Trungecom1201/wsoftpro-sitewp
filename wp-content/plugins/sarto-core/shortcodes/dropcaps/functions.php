<?php

if ( ! function_exists( 'sarto_core_add_dropcaps_shortcodes' ) ) {
	function sarto_core_add_dropcaps_shortcodes( $shortcodes_class_name ) {
		$shortcodes = array(
			'SartoCore\CPT\Shortcodes\Dropcaps\Dropcaps'
		);
		
		$shortcodes_class_name = array_merge( $shortcodes_class_name, $shortcodes );
		
		return $shortcodes_class_name;
	}
	
	add_filter( 'sarto_core_filter_add_vc_shortcode', 'sarto_core_add_dropcaps_shortcodes' );
}