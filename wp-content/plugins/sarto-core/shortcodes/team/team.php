<?php
namespace SartoCore\CPT\Shortcodes\Team;

use SartoCore\lib;

class Team implements lib\ShortcodeInterface
{
    private $base;

    public function __construct() {
        $this->base = 'edgtf_team';

        add_action('vc_before_init', array($this, 'vcMap'));
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        $team_social_icons_array = array();

        for ($x = 1; $x < 6; $x++) {
            $teamIconCollections = sarto_edge_icon_collections()->getCollectionsWithSocialIcons();
            foreach ($teamIconCollections as $collection_key => $collection) {

                $team_social_icons_array[] = array(
                    'type'       => 'dropdown',
                    'heading'    => esc_html__('Social Icon ', 'sarto-core') . $x,
                    'param_name' => 'team_social_' . $collection->param . '_' . $x,
                    'value'      => $collection->getSocialIconsArrayVC(),
                    'dependency' => Array('element' => 'team_social_icon_pack', 'value' => array($collection_key))
                );
            }

            $team_social_icons_array[] = array(
                'type'       => 'textfield',
                'heading'    => esc_html__('Social Icon ', 'sarto-core') . $x . esc_html__(' Link', 'sarto-core'),
                'param_name' => 'team_social_icon_' . $x . '_link',
                'dependency' => array(
                    'element' => 'team_social_icon_pack',
                    'value'   => sarto_edge_icon_collections()->getIconCollectionsKeys()
                )
            );

            $team_social_icons_array[] = array(
                'type'       => 'dropdown',
                'heading'    => esc_html__('Social Icon ', 'sarto-core') . $x . esc_html__(' Target', 'sarto-core'),
                'param_name' => 'team_social_icon_' . $x . '_target',
                'value'      => array(
                    esc_html__('Same Window', 'sarto-core') => '_self',
                    esc_html__('New Window', 'sarto-core')  => '_blank'
                ),
                'dependency' => Array('element' => 'team_social_icon_' . $x . '_link', 'not_empty' => true)
            );
        }

        if (function_exists('vc_map')) {
            vc_map(
                array(
                    'name'                      => esc_html__('Team', 'sarto-core'),
                    'base'                      => $this->base,
                    'category'                  => esc_html__('by SARTO', 'sarto-core'),
                    'icon'                      => 'icon-wpb-team extended-custom-icon',
                    'allowed_container_element' => 'vc_row',
                    'params'                    => array_merge(
                        array(
                            array(
                                'type'        => 'dropdown',
                                'param_name'  => 'type',
                                'heading'     => esc_html__('Type', 'sarto-core'),
                                'value'       => array(
                                    esc_html__('Info Below Image', 'sarto-core')    => 'info-below-image',
                                    esc_html__('Info On Image Hover', 'sarto-core') => 'info-on-image'
                                ),
                                'save_always' => true
                            ),
                            array(
                                'type'        => 'textfield',
                                'param_name'  => 'holder_padding',
                                'heading'     => esc_html__('Holder padding', 'sarto-core'),
                                'description' => esc_html__('For example: 20px 15px 20px 10px', 'sarto-core'),
                                'dependency'  => array('element' => 'type', 'value' => 'info-on-image')
                            ),
                            array(
                                'type'       => 'attach_image',
                                'param_name' => 'team_image',
                                'heading'    => esc_html__('Image', 'sarto-core')
                            ),
                            array(
                                'type'       => 'textfield',
                                'param_name' => 'team_name',
                                'heading'    => esc_html__('Name', 'sarto-core'),
                            ),
                            array(
                                'type'        => 'dropdown',
                                'param_name'  => 'team_name_tag',
                                'heading'     => esc_html__('Name Tag', 'sarto-core'),
                                'value'       => array_flip(sarto_edge_get_title_tag(true)),
                                'save_always' => true,
                                'dependency'  => array('element' => 'team_name', 'not_empty' => true)
                            ),
                            array(
                                'type'        => 'textfield',
                                'param_name'  => 'team_name_break_words',
                                'heading'     => esc_html__('Position of Line Break', 'sarto-core'),
                                'description' => esc_html__('Enter the position of the word after which you would like to create a line break (e.g. if you would like the line break after the 3rd word, you would enter "3")', 'sarto-core'),
                                'dependency'  => array('element' => 'team_name', 'not_empty' => true),
                            ),
                            array(
                                'type'       => 'colorpicker',
                                'param_name' => 'team_name_color',
                                'heading'    => esc_html__('Name Color', 'sarto-core'),
                                'dependency' => array('element' => 'team_name', 'not_empty' => true)
                            ),
                            array(
                                'type'       => 'textfield',
                                'param_name' => 'team_position',
                                'heading'    => esc_html__('Position', 'sarto-core')
                            ),
                            array(
                                'type'       => 'colorpicker',
                                'param_name' => 'team_position_color',
                                'heading'    => esc_html__('Position Color', 'sarto-core'),
                                'dependency' => array('element' => 'team_position', 'not_empty' => true)
                            ),
                            array(
                                'type'       => 'textfield',
                                'param_name' => 'team_text',
                                'heading'    => esc_html__('Text', 'sarto-core')
                            ),
                            array(
                                'type'       => 'colorpicker',
                                'param_name' => 'team_text_color',
                                'heading'    => esc_html__('Text Color', 'sarto-core'),
                                'dependency' => array('element' => 'team_text', 'not_empty' => true)
                            ),
                            array(
                                'type'        => 'dropdown',
                                'param_name'  => 'team_social_icon_pack',
                                'heading'     => esc_html__('Social Icon Pack', 'sarto-core'),
                                'value'       => array_merge(array('' => ''), sarto_edge_icon_collections()->getIconCollectionsVCExclude('linea_icons')),
                                'save_always' => true
                            ),
                        ),
                        $team_social_icons_array
                    )
                )
            );
        }
    }

    public function render($atts, $content = null) {
        $args = array(
            'type'                  => 'info-below-image',
            'holder_padding'        => '',
            'team_image'            => '',
            'team_name'             => '',
            'team_name_break_words' => '',
            'team_name_tag'         => 'h4',
            'team_name_color'       => '',
            'team_position'         => '',
            'team_position_color'   => '',
            'team_text'             => '',
            'team_text_color'       => '',
            'team_social_icon_pack' => ''
        );

        $team_social_icons_form_fields = array();
        $number_of_social_icons = 5;

        for ($x = 1; $x <= $number_of_social_icons; $x++) {

            foreach (sarto_edge_icon_collections()->iconCollections as $collection_key => $collection) {
                $team_social_icons_form_fields['team_social_' . $collection->param . '_' . $x] = '';
            }

            $team_social_icons_form_fields['team_social_icon_' . $x . '_link'] = '';
            $team_social_icons_form_fields['team_social_icon_' . $x . '_target'] = '';
        }

        $args = array_merge($args, $team_social_icons_form_fields);

        $params = shortcode_atts($args, $atts);

        $params['number_of_social_icons'] = 5;

        $params['type'] = !empty($params['type']) ? $params['type'] : $args['type'];
        $params['holder_classes'] = $this->getHolderClasses($params);
        $params['inner_holder_styles'] = $this->getInnerHolderStyles($params);
        $params['team_name'] = $this->getModifiedTeamName($params);
        $params['team_name_tag'] = !empty($params['team_name_tag']) ? $params['team_name_tag'] : $args['team_name_tag'];
        $params['team_social_icons'] = $this->getTeamSocialIcons($params);
        $params['team_name_styles'] = $this->getTeamNameStyles($params);
        $params['team_position_styles'] = $this->getTeamPositionStyles($params);
        $params['team_text_styles'] = $this->getTeamTextStyles($params);

        //Get HTML from template based on type of team
        $html = sarto_core_get_shortcode_module_template_part('templates/' . $params['type'], 'team', '', $params);

        return $html;
    }

    private function getHolderClasses($params) {
        $holderClasses = array();

        $holderClasses[] = !empty($params['type']) ? 'edgtf-team-' . $params['type'] : '';

        return implode(' ', $holderClasses);
    }

    private function getInnerHolderStyles($params) {
        $styles = array();

        if (!empty($params['holder_padding'])) {
            $styles[] = 'padding: ' . $params['holder_padding'];
        }

        return implode(';', $styles);
    }

    private function getTeamSocialIcons($params) {
        extract($params);
        $social_icons = array();

        if ($team_social_icon_pack !== '') {

            $icon_pack = sarto_edge_icon_collections()->getIconCollection($team_social_icon_pack);
            $team_social_icon_type_label = 'team_social_' . $icon_pack->param;
            $team_social_icon_param_label = $icon_pack->param;

            for ($i = 1; $i <= $params['number_of_social_icons']; $i++) {

                $team_social_icon = ${$team_social_icon_type_label . '_' . $i};
                $team_social_link = ${'team_social_icon_' . $i . '_link'};
                $team_social_target = ${'team_social_icon_' . $i . '_target'};

                if ($team_social_icon !== '') {

                    $team_icon_params = array();
                    $team_icon_params['icon_pack'] = $team_social_icon_pack;
                    $team_icon_params[$team_social_icon_param_label] = $team_social_icon;
                    $team_icon_params['link'] = ($team_social_link !== '') ? $team_social_link : '';
                    $team_icon_params['target'] = ($team_social_target !== '') ? $team_social_target : '';

                    $social_icons[] = sarto_edge_execute_shortcode('edgtf_icon', $team_icon_params);
                }
            }
        }

        return $social_icons;
    }

    private function getModifiedTeamName($params) {
        $team_name = $params['team_name'];
        $team_name_break_words = str_replace(' ', '', $params['team_name_break_words']);

        if (!empty($team_name)) {
            $split_team_name = explode(' ', $team_name);

            if (!empty($team_name_break_words)) {
                if (!empty($split_team_name[$team_name_break_words - 1])) {
                    $split_team_name[$team_name_break_words - 1] = $split_team_name[$team_name_break_words - 1] . '<br />';
                }
            }

            $team_name = implode(' ', $split_team_name);
        }

        return $team_name;
    }

    private function getTeamNameStyles($params) {
        $styles = array();

        if (!empty($params['team_name_color'])) {
            $styles[] = 'color: ' . $params['team_name_color'];
        }

        return implode(';', $styles);
    }

    private function getTeamPositionStyles($params) {
        $styles = array();

        if (!empty($params['team_position_color'])) {
            $styles[] = 'color: ' . $params['team_position_color'];
        }

        return implode(';', $styles);
    }

    private function getTeamTextStyles($params) {
        $styles = array();

        if (!empty($params['team_text_color'])) {
            $styles[] = 'color: ' . $params['team_text_color'];
        }

        return implode(';', $styles);
    }
}