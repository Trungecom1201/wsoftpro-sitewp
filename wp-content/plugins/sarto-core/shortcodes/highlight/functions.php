<?php

if ( ! function_exists( 'sarto_core_add_highlight_shortcodes' ) ) {
	function sarto_core_add_highlight_shortcodes( $shortcodes_class_name ) {
		$shortcodes = array(
			'SartoCore\CPT\Shortcodes\Highlight\Highlight'
		);
		
		$shortcodes_class_name = array_merge( $shortcodes_class_name, $shortcodes );
		
		return $shortcodes_class_name;
	}
	
	add_filter( 'sarto_core_filter_add_vc_shortcode', 'sarto_core_add_highlight_shortcodes' );
}