<?php
/*
Plugin Name: Sarto Instagram Feed
Description: Plugin that adds Instagram feed functionality to our theme
Author: Edge Themes
Version: 1.0
*/

include_once 'load.php';

if ( ! function_exists( 'sarto_instagram_theme_installed' ) ) {
	/**
	 * Checks whether theme is installed or not
	 * @return bool
	 */
	function sarto_instagram_theme_installed() {
		return defined( 'EDGE_ROOT' );
	}
}

if(!function_exists('sarto_instagram_feed_text_domain')) {
    /**
     * Loads plugin text domain so it can be used in translation
     */
    function sarto_instagram_feed_text_domain() {
        load_plugin_textdomain('sarto-instagram-feed', false, SARTO_INSTAGRAM_REL_PATH.'/languages');
    }

    add_action('plugins_loaded', 'sarto_instagram_feed_text_domain');
}