<?php

define( 'SARTO_TWITTER_FEED_VERSION', '1.0' );
define( 'SARTO_TWITTER_ABS_PATH', dirname( __FILE__ ) );
define( 'SARTO_TWITTER_REL_PATH', dirname( plugin_basename( __FILE__ ) ) );
define( 'SARTO_TWITTER_URL_PATH', plugin_dir_url( __FILE__ ) );
define( 'SARTO_TWITTER_ASSETS_PATH', SARTO_TWITTER_ABS_PATH . '/assets' );
define( 'SARTO_TWITTER_ASSETS_URL_PATH', SARTO_TWITTER_URL_PATH . 'assets' );
define( 'SARTO_TWITTER_SHORTCODES_PATH', SARTO_TWITTER_ABS_PATH . '/shortcodes' );
define( 'SARTO_TWITTER_SHORTCODES_URL_PATH', SARTO_TWITTER_URL_PATH . 'shortcodes' );