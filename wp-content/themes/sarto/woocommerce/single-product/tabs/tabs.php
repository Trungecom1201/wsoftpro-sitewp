<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

    <div class="woocommerce-tabs wc-tabs-wrapper">
        <ul class="tabs wc-tabs" role="tablist">
            <?php foreach ( $tabs as $key => $tab ) : ?>
                <li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-<?php echo esc_attr( $key ); ?>" role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
                    <a href="#tab-<?php echo esc_attr( $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php foreach ( $tabs as $key => $tab ) : ?>
            <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab" id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
                <?php if ( isset( $tab['callback'] ) ) { call_user_func( $tab['callback'], $key, $tab ); } ?>
            </div>
        <?php endforeach; ?>
    </div>

<?php endif; ?>

<div class="reason-choose">
    <p class="reason-title">Reason to choose</p>
    <h3 class="sub-reason-title">Magetop</h3>

    <div class="reason-wrap">
        <div class="reason-item">
            <div class="img-wrap">
                <img src="https://www.magetop.com/media/magetop2020/svg/money-back.svg" alt="30 days money back">
            </div>
            <p>30-day money-back</p>
        </div>
        <div class="reason-item">
            <div class="img-wrap">
                <img src="https://www.magetop.com/media/magetop2020/svg/24-hours.svg" alt="FREE lifetime updates">
            </div>
            <p>FREE lifetime updates</p>
        </div>
        <div class="reason-item">
            <div class="img-wrap">
                <img src="https://www.magetop.com/media/magetop2020/svg/help.svg" alt="90 days FREE support">
            </div>
            <p>90 days FREE support</p>
        </div>
        <div class="reason-item">
            <div class="img-wrap">
                <img src="https://www.magetop.com/media/magetop2020/svg/open-source.svg" alt="100% Open Source">
            </div>
            <p>100% Open Source</p>
        </div>
    </div>
</div>