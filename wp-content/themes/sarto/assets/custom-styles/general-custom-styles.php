<?php

if(!function_exists('sarto_edge_design_styles')) {
    /**
     * Generates general custom styles
     */
    function sarto_edge_design_styles() {
	    $font_family = sarto_edge_options()->getOptionValue( 'google_fonts' );
	    if ( ! empty( $font_family ) && sarto_edge_is_font_option_valid( $font_family ) ) {
		    $font_family_selector = array(
			    'body'
		    );
		    echo sarto_edge_dynamic_css( $font_family_selector, array( 'font-family' => sarto_edge_get_font_option_val( $font_family ) ) );
	    }

		$first_main_color = sarto_edge_options()->getOptionValue('first_color');
        if(!empty($first_main_color)) {
            $color_selector = array(

	            'a:hover',
	            'h1 a:hover',
	            'h2 a:hover',
	            'h3 a:hover',
	            'h4 a:hover',
	            'h5 a:hover',
	            'h6 a:hover',
	            'p a:hover',
	            'blockquote:before',
	            '.edgtf-comment-holder .edgtf-comment-text .comment-edit-link:hover',
	            '.edgtf-comment-holder .edgtf-comment-text .comment-reply-link:hover',
	            '.edgtf-comment-holder .edgtf-comment-text .replay:hover',
	            '.edgtf-comment-holder .edgtf-comment-text #cancel-comment-reply-link',
	            '.edgtf-two-columns-form.edgtf-newsletter input.wpcf7-form-control.wpcf7-submit:hover',
	            '.edgtf-owl-slider .owl-nav .owl-next:hover',
	            '.edgtf-owl-slider .owl-nav .owl-prev:hover',
	            '.widget .edgtf-widget-title',
	            '.widget.widget_rss .edgtf-widget-title .rsswidget:hover',
	            '.widget.widget_search button:hover',
	            '.widget.edgtf-social-icons-group-widget a:hover',
	            '.edgtf-side-menu .widget a:hover',
	            '.edgtf-side-menu .widget.widget_rss .edgtf-footer-widget-title .rsswidget:hover',
	            '.edgtf-side-menu .widget.widget_search button:hover',
	            '.edgtf-side-menu .widget.widget_tag_cloud a:hover',
	            '.edgtf-page-footer .widget a:hover',
	            '.edgtf-page-footer .widget.widget_rss .edgtf-footer-widget-title .rsswidget:hover',
	            '.edgtf-page-footer .widget.widget_search button:hover',
	            '.edgtf-page-footer .widget.widget_tag_cloud a:hover',
	            '.edgtf-top-bar a:hover',
	            '.edgtf-top-bar .widget.edgtf-social-icons-group-widget a:hover',
	            '.edgtf-top-bar .widget.widget_icl_lang_sel_widget li a:hover',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-standard li .edgtf-twitter-icon',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-tweet-text a',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-tweet-text span',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-standard li .edgtf-tweet-text a:hover',
	            '.widget.widget_edgtf_twitter_widget .edgtf-twitter-widget.edgtf-twitter-slider li .edgtf-twitter-icon i',
	            'body .pp_pic_holder a.pp_next',
	            'body .pp_pic_holder a.pp_previous',
	            '.edgtf-top-bar #lang_sel li .lang_sel_sel:hover',
	            '.edgtf-top-bar #lang_sel ul ul a:hover',
	            '.edgtf-blog-holder article.sticky .edgtf-post-title a',
	            '.edgtf-blog-holder article .edgtf-post-info-top>div a:hover',
	            '.edgtf-blog-holder article .edgtf-post-info-bottom .edgtf-post-info-bottom-left a:hover',
	            '.edgtf-blog-holder article .edgtf-post-info-bottom .edgtf-post-info-bottom-right a:hover',
	            '.edgtf-blog-holder article .edgtf-post-info-bottom .edgtf-post-read-more-button a',
	            '.edgtf-blog-pagination ul li a.edgtf-pag-active',
	            '.edgtf-blog-pagination ul li a:hover',
	            '.edgtf-bl-standard-pagination ul li.edgtf-bl-pag-active a',
	            '.edgtf-author-description .edgtf-author-description-text-holder .edgtf-author-name a:hover',
	            '.edgtf-author-description .edgtf-author-description-text-holder .edgtf-author-social-icons a:hover',
	            '.edgtf-blog-single-navigation a:hover .edgtf-nav-title-inner',
	            '.edgtf-blog-single-navigation .edgtf-blog-single-share .edgtf-social-share-holder ul li a',
	            '.edgtf-single-links-pages .edgtf-single-links-pages-inner>a:hover',
	            '.edgtf-single-links-pages .edgtf-single-links-pages-inner>span',
	            '.edgtf-blog-list-holder .edgtf-bli-info>div a:hover',
	            '.edgtf-blog-list-holder.edgtf-bl-boxed .edgtf-bli-content .edgtf-post-read-more-button a',
	            '.edgtf-main-menu ul li a:hover',
	            '.edgtf-fullscreen-menu-opener.edgtf-fm-opened',
	            '.edgtf-fullscreen-menu-opener:hover',
	            'nav.edgtf-fullscreen-menu ul li ul li.current-menu-ancestor>a',
	            'nav.edgtf-fullscreen-menu ul li ul li.current-menu-item>a',
	            'nav.edgtf-fullscreen-menu>ul>li.edgtf-active-item>a',
	            '.edgtf-mobile-header .edgtf-mobile-menu-opener.edgtf-mobile-menu-opened a',
	            '.edgtf-mobile-header .edgtf-mobile-nav .edgtf-grid>ul>li.edgtf-active-item>a',
	            '.edgtf-mobile-header .edgtf-mobile-nav .edgtf-grid>ul>li.edgtf-active-item>h6',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul li a:hover',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul li h6:hover',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-ancestor>a',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-ancestor>h6',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-item>a',
	            '.edgtf-mobile-header .edgtf-mobile-nav ul ul li.current-menu-item>h6',
	            '.edgtf-search-page-holder article.sticky .edgtf-post-title a',
	            '.edgtf-search-cover .edgtf-search-close',
	            '.edgtf-side-menu-button-opener.opened',
	            '.edgtf-side-menu-button-opener:hover',
	            '.edgtf-side-menu a.edgtf-close-side-menu',
	            '.edgtf-pl-standard-pagination ul li.edgtf-pl-pag-active a',
	            '.edgtf-portfolio-list-holder.edgtf-pl-gallery-overlay-with-crosshair article .edgtf-pli-text .edgtf-pli-category-holder a:hover',
	            '.edgtf-portfolio-list-holder.edgtf-pl-gallery-overlay article .edgtf-pli-text .edgtf-pli-category-holder a:hover',
	            '.edgtf-testimonials-holder.edgtf-testimonials-image-pagination .edgtf-testimonials-image-pagination-inner .edgtf-testimonials-author-job',
	            '.edgtf-testimonials-holder.edgtf-testimonials-image-pagination.edgtf-testimonials-light .owl-nav .owl-next:hover',
	            '.edgtf-testimonials-holder.edgtf-testimonials-image-pagination.edgtf-testimonials-light .owl-nav .owl-prev:hover',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard-with-quote.edgtf-testimonials-dark .owl-nav .owl-next',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard-with-quote.edgtf-testimonials-dark .owl-nav .owl-prev',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard.edgtf-testimonials-dark .owl-nav .owl-next',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard.edgtf-testimonials-dark .owl-nav .owl-prev',
	            '.edgtf-reviews-per-criteria .edgtf-item-reviews-average-rating',
	            '.edgtf-banner-holder .edgtf-banner-link-text .edgtf-banner-link-hover span',
	            '.edgtf-btn.edgtf-btn-simple',
	            '.edgtf-btn.edgtf-btn-outline',
	            '.edgtf-countdown .countdown-row .countdown-section .countdown-period',
	            '.edgtf-price-table .edgtf-pt-inner ul li.edgtf-pt-title-holder .edgtf-pt-title',
	            '.edgtf-price-table.edgtf-active .edgtf-pt-inner ul li.edgtf-pt-prices .edgtf-pt-price',
	            '.edgtf-price-table.edgtf-active .edgtf-pt-inner ul li.edgtf-pt-prices .edgtf-pt-value',
	            '.edgtf-process-holder .edgtf-process-item-holder .edgtf-pi-number-holder .edgtf-pi-arrow',
	            '.edgtf-social-share-holder.edgtf-dropdown .edgtf-social-share-dropdown-opener:hover',
	            '.edgtf-tabs.edgtf-tabs-standard .edgtf-tabs-nav li a:hover',
	            '.edgtf-tabs.edgtf-tabs-standard .edgtf-tabs-nav li.ui-state-active a',
	            '.edgtf-tabs.edgtf-tabs-standard .edgtf-tabs-nav li.ui-state-hover a',
	            '.edgtf-twitter-list-holder .edgtf-tweet-text a:hover',
	            '.edgtf-twitter-list-holder .edgtf-twitter-profile a:hover',
	            '.edgtf-top-bar .widget a:hover'

            );

            $woo_color_selector = array();
            if(sarto_edge_is_woocommerce_installed()) {
                $woo_color_selector = array(
	                '.woocommerce-pagination .page-numbers li a.current',
	                '.woocommerce-pagination .page-numbers li a:hover',
	                '.woocommerce-pagination .page-numbers li span.current',
	                '.woocommerce-pagination .page-numbers li span:hover',
	                '.woocommerce-pagination .page-numbers li a.next:hover',
	                '.woocommerce-pagination .page-numbers li a.prev:hover',
	                '.woocommerce-page .edgtf-content .edgtf-quantity-buttons .edgtf-quantity-minus:hover',
	                '.woocommerce-page .edgtf-content .edgtf-quantity-buttons .edgtf-quantity-plus:hover',
	                'div.woocommerce .edgtf-quantity-buttons .edgtf-quantity-minus:hover',
	                'div.woocommerce .edgtf-quantity-buttons .edgtf-quantity-plus:hover',
	                '.edgtf-woo-single-page .edgtf-single-product-summary .product_meta>span a:hover',
	                '.edgtf-woo-single-page .woocommerce-tabs ul.tabs>li a:hover',
	                '.edgtf-woo-single-page .woocommerce-tabs ul.tabs>li.active a',
	                '.edgtf-shopping-cart-dropdown .edgtf-cart-bottom .edgtf-cart-checkout:hover',
	                '.edgtf-shopping-cart-dropdown .edgtf-cart-bottom .edgtf-view-cart:hover',
	                '.widget.woocommerce.widget_layered_nav ul li.chosen a'
                );
            }

            $color_selector = array_merge($color_selector, $woo_color_selector);

	        $color_important_selector = array(
		        '.edgtf-portfolio-single-holder .edgtf-ps-info-holder .edgtf-social-share-holder a:hover',
	        );

            $background_color_selector = array(


	            '.edgtf-sarto-spinner .edgtf-spinner-mask-default',
	            '.edgtf-st-loader .pulse',
	            '.edgtf-st-loader .double_pulse .double-bounce1',
	            '.edgtf-st-loader .double_pulse .double-bounce2',
	            '.edgtf-st-loader .cube',
	            '.edgtf-st-loader .rotating_cubes .cube1',
	            '.edgtf-st-loader .rotating_cubes .cube2',
	            '.edgtf-st-loader .stripes>div',
	            '.edgtf-st-loader .wave>div',
	            '.edgtf-st-loader .two_rotating_circles .dot1',
	            '.edgtf-st-loader .two_rotating_circles .dot2',
	            '.edgtf-st-loader .five_rotating_circles .container1>div',
	            '.edgtf-st-loader .five_rotating_circles .container2>div',
	            '.edgtf-st-loader .five_rotating_circles .container3>div',
	            '.edgtf-st-loader .lines .line1',
	            '.edgtf-st-loader .lines .line2',
	            '.edgtf-st-loader .lines .line3',
	            '.edgtf-st-loader .lines .line4',
	            '#submit_comment',
	            '.post-password-form input[type=submit]',
	            'input.wpcf7-form-control.wpcf7-submit',
	            '#edgtf-back-to-top>span',
	            '.widget #wp-calendar td#today',
	            '.widget.widget_tag_cloud a',
	            '.edgtf-social-icons-group-widget.edgtf-square-icons .edgtf-social-icon-widget-holder:hover',
	            '.edgtf-social-icons-group-widget.edgtf-square-icons.edgtf-light-skin .edgtf-social-icon-widget-holder:hover',
	            '.edgtf-blog-holder article.format-audio .edgtf-blog-audio-holder .mejs-container .mejs-controls>.mejs-time-rail .mejs-time-total .mejs-time-current',
	            '.edgtf-blog-holder article.format-audio .edgtf-blog-audio-holder .mejs-container .mejs-controls>a.mejs-horizontal-volume-slider .mejs-horizontal-volume-current',
	            '.edgtf-search-fade .edgtf-fullscreen-with-sidebar-search-holder .edgtf-fullscreen-search-table',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard-with-quote.edgtf-testimonials-dark .owl-dots .owl-dot.active span',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard-with-quote.edgtf-testimonials-dark .owl-dots .owl-dot:hover span',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard.edgtf-testimonials-dark .owl-dots .owl-dot.active span',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard.edgtf-testimonials-dark .owl-dots .owl-dot:hover span',
	            '.edgtf-accordion-holder.edgtf-ac-boxed .edgtf-accordion-title.ui-state-active',
	            '.edgtf-accordion-holder.edgtf-ac-boxed .edgtf-accordion-title.ui-state-hover',
	            '.edgtf-btn.edgtf-btn-solid',
	            '#edgtf-content-switch-intro .edgtf-csi-mask-default',
	            '.edgtf-icon-shortcode.edgtf-circle',
	            '.edgtf-icon-shortcode.edgtf-dropcaps.edgtf-circle',
	            '.edgtf-icon-shortcode.edgtf-square',
	            '.edgtf-price-table.edgtf-active .edgtf-pt-inner ul li.edgtf-pt-title-holder',
	            '.edgtf-process-holder .edgtf-process-item-holder .edgtf-pi-number-holder:after',
	            '.edgtf-progress-bar .edgtf-pb-content-holder .edgtf-pb-content',
	            '.edgtf-tabs.edgtf-tabs-standard .edgtf-tabs-nav li.ui-state-active:after',
	            '.edgtf-tabs.edgtf-tabs-standard .edgtf-tabs-nav li.ui-state-hover:after',
	            '.edgtf-tabs.edgtf-tabs-boxed .edgtf-tabs-nav li.ui-state-active a',
	            '.edgtf-tabs.edgtf-tabs-boxed .edgtf-tabs-nav li.ui-state-hover a'

            );

            $woo_background_color_selector = array();
            if(sarto_edge_is_woocommerce_installed()) {
                $woo_background_color_selector = array(
	                '.woocommerce-page .edgtf-content .wc-forward:not(.added_to_cart):not(.checkout-button)',
	                '.woocommerce-page .edgtf-content a.added_to_cart',
	                '.woocommerce-page .edgtf-content a.button',
	                '.woocommerce-page .edgtf-content button[type=submit]:not(.edgtf-search-submit)',
	                '.woocommerce-page .edgtf-content input[type=submit]',
	                'div.woocommerce .wc-forward:not(.added_to_cart):not(.checkout-button)',
	                'div.woocommerce a.added_to_cart',
	                'div.woocommerce a.button',
	                'div.woocommerce button[type=submit]:not(.edgtf-search-submit)',
	                'div.woocommerce input[type=submit]',
	                '.edgtf-shopping-cart-holder .edgtf-header-cart .edgtf-cart-number',
	                '.widget.woocommerce.widget_price_filter .price_slider_wrapper .ui-widget-content .ui-slider-range',
	                '.widget.woocommerce.widget_price_filter .price_slider_amount .button',
	                '.widget.woocommerce.widget_product_tag_cloud .tagcloud a',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-default-skin .added_to_cart',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-default-skin .button',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-light-skin .added_to_cart:hover',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-light-skin .button:hover',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-dark-skin .added_to_cart:hover',
	                '.edgtf-plc-holder .edgtf-plc-item .edgtf-plc-add-to-cart.edgtf-dark-skin .button:hover',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-default-skin .added_to_cart',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-default-skin .button',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-light-skin .added_to_cart:hover',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-light-skin .button:hover',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-dark-skin .added_to_cart:hover',
	                '.edgtf-pl-holder .edgtf-pli-inner .edgtf-pli-text-inner .edgtf-pli-add-to-cart.edgtf-dark-skin .button:hover'
                );
            }

            $background_color_selector = array_merge($background_color_selector, $woo_background_color_selector);

	        $background_color_important_selector = array(
		        '.edgtf-btn.edgtf-btn-outline:not(.edgtf-btn-custom-hover-bg):hover'
	        );

            $border_color_selector = array(
	            '.edgtf-st-loader .pulse_circles .ball',
	            '.edgtf-two-columns-form.edgtf-newsletter .wpcf7-form-control.wpcf7-text',
	            '.edgtf-two-columns-form.edgtf-newsletter input.wpcf7-form-control.wpcf7-submit',
	            '.edgtf-owl-slider+.edgtf-slider-thumbnail>.edgtf-slider-thumbnail-item.active img',
	            '#edgtf-back-to-top>span',
	            '.widget.widget_search form>div',
	            '.edgtf-side-menu .widget.widget_nav_menu li a:before',
	            '.edgtf-page-footer .widget.widget_nav_menu li a:before',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard-with-quote.edgtf-testimonials-dark .owl-dots .owl-dot span',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard-with-quote.edgtf-testimonials-dark .owl-dots .owl-dot.active span',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard-with-quote.edgtf-testimonials-dark .owl-dots .owl-dot:hover span',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard.edgtf-testimonials-dark .owl-dots .owl-dot span',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard.edgtf-testimonials-dark .owl-dots .owl-dot.active span',
	            '.edgtf-testimonials-holder.edgtf-testimonials-standard.edgtf-testimonials-dark .owl-dots .owl-dot:hover span',
	            '.edgtf-btn.edgtf-btn-outline'
            );

	        $border_color_important_selector = array(
		        '.edgtf-btn.edgtf-btn-outline:not(.edgtf-btn-custom-border-hover):hover',
	        );

	        $border_color_top_selector = array(
				'.edgtf-drop-down .second',
		        '.edgtf-drop-down .narrow .second .inner ul li ul',
		        '.edgtf-shopping-cart-dropdown'
	        );

	        $border_color_right_selector = array(
		        '.edgtf-two-columns-form.edgtf-newsletter .edgtf-column-inner:first-child .wpcf7-form-control.wpcf7-text'
	        );

	        $border_color_bottom_selector = array(
				'.edgtf-blog-pagination ul li a.edgtf-pag-active,.edgtf-blog-pagination ul li a:hover',
		        '.woocommerce-pagination .page-numbers li a.current',
		        '.woocommerce-pagination .page-numbers li a:hover',
		        '.woocommerce-pagination .page-numbers li span.current',
		        '.woocommerce-pagination .page-numbers li span:hover',
		        '.edgtf-woo-single-page .woocommerce-tabs ul.tabs>li a:hover',
		        '.edgtf-woo-single-page .woocommerce-tabs ul.tabs>li.active a'
	        );

	        $border_color_left_selector = array(
				'.edgtf-header-vertical .edgtf-vertical-menu.edgtf-vertical-dropdown-side .second ul.edgtf-float-open',
	        );

            echo sarto_edge_dynamic_css($color_selector, array('color' => $first_main_color));
	        echo sarto_edge_dynamic_css($color_important_selector, array('color' => $first_main_color.'!important'));
	        echo sarto_edge_dynamic_css($background_color_selector, array('background-color' => $first_main_color));
	        echo sarto_edge_dynamic_css($background_color_important_selector, array('background-color' => $first_main_color.'!important'));
	        echo sarto_edge_dynamic_css($border_color_selector, array('border-color' => $first_main_color));
	        echo sarto_edge_dynamic_css($border_color_important_selector, array('border-color' => $first_main_color.'!important'));
	        echo sarto_edge_dynamic_css($border_color_top_selector, array('border-color' => $first_main_color));
	        echo sarto_edge_dynamic_css($border_color_right_selector, array('border-color' => $first_main_color));
	        echo sarto_edge_dynamic_css($border_color_bottom_selector, array('border-color' => $first_main_color));
	        echo sarto_edge_dynamic_css($border_color_left_selector, array('border-color' => $first_main_color));
        }
	
	    $page_background_color = sarto_edge_options()->getOptionValue( 'page_background_color' );
	    if ( ! empty( $page_background_color ) ) {
		    $background_color_selector = array(
			    'body',
			    '.edgtf-content'
		    );
		    echo sarto_edge_dynamic_css( $background_color_selector, array( 'background-color' => $page_background_color ) );
	    }
	
	    $selection_color = sarto_edge_options()->getOptionValue( 'selection_color' );
	    if ( ! empty( $selection_color ) ) {
		    echo sarto_edge_dynamic_css( '::selection', array( 'background' => $selection_color ) );
		    echo sarto_edge_dynamic_css( '::-moz-selection', array( 'background' => $selection_color ) );
	    }
	
	    $preload_background_styles = array();
	
	    if ( sarto_edge_options()->getOptionValue( 'preload_pattern_image' ) !== "" ) {
		    $preload_background_styles['background-image'] = 'url(' . sarto_edge_options()->getOptionValue( 'preload_pattern_image' ) . ') !important';
	    }
	
	    echo sarto_edge_dynamic_css( '.edgtf-preload-background', $preload_background_styles );
    }

    add_action('sarto_edge_style_dynamic', 'sarto_edge_design_styles');
}

if ( ! function_exists( 'sarto_edge_content_styles' ) ) {
	function sarto_edge_content_styles() {
		$content_style = array();
		
		$padding = sarto_edge_options()->getOptionValue( 'content_padding' );
		if ( $padding !== '' ) {
			$content_style['padding'] = $padding;
		}
		
		$content_selector = array(
			'.edgtf-content .edgtf-content-inner > .edgtf-full-width > .edgtf-full-width-inner',
		);
		
		echo sarto_edge_dynamic_css( $content_selector, $content_style );
		
		$content_style_in_grid = array();
		
		$padding_in_grid = sarto_edge_options()->getOptionValue( 'content_padding_in_grid' );
		if ( $padding_in_grid !== '' ) {
			$content_style_in_grid['padding'] = $padding_in_grid;
		}
		
		$content_selector_in_grid = array(
			'.edgtf-content .edgtf-content-inner > .edgtf-container > .edgtf-container-inner',
		);
		
		echo sarto_edge_dynamic_css( $content_selector_in_grid, $content_style_in_grid );
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_content_styles' );
}

if ( ! function_exists( 'sarto_edge_h1_styles' ) ) {
	function sarto_edge_h1_styles() {
		$margin_top    = sarto_edge_options()->getOptionValue( 'h1_margin_top' );
		$margin_bottom = sarto_edge_options()->getOptionValue( 'h1_margin_bottom' );
		
		$item_styles = sarto_edge_get_typography_styles( 'h1' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = sarto_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = sarto_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h1'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo sarto_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_h1_styles' );
}

if ( ! function_exists( 'sarto_edge_h2_styles' ) ) {
	function sarto_edge_h2_styles() {
		$margin_top    = sarto_edge_options()->getOptionValue( 'h2_margin_top' );
		$margin_bottom = sarto_edge_options()->getOptionValue( 'h2_margin_bottom' );
		
		$item_styles = sarto_edge_get_typography_styles( 'h2' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = sarto_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = sarto_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h2'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo sarto_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_h2_styles' );
}

if ( ! function_exists( 'sarto_edge_h3_styles' ) ) {
	function sarto_edge_h3_styles() {
		$margin_top    = sarto_edge_options()->getOptionValue( 'h3_margin_top' );
		$margin_bottom = sarto_edge_options()->getOptionValue( 'h3_margin_bottom' );
		
		$item_styles = sarto_edge_get_typography_styles( 'h3' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = sarto_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = sarto_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h3'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo sarto_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_h3_styles' );
}

if ( ! function_exists( 'sarto_edge_h4_styles' ) ) {
	function sarto_edge_h4_styles() {
		$margin_top    = sarto_edge_options()->getOptionValue( 'h4_margin_top' );
		$margin_bottom = sarto_edge_options()->getOptionValue( 'h4_margin_bottom' );
		
		$item_styles = sarto_edge_get_typography_styles( 'h4' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = sarto_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = sarto_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h4'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo sarto_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_h4_styles' );
}

if ( ! function_exists( 'sarto_edge_h5_styles' ) ) {
	function sarto_edge_h5_styles() {
		$margin_top    = sarto_edge_options()->getOptionValue( 'h5_margin_top' );
		$margin_bottom = sarto_edge_options()->getOptionValue( 'h5_margin_bottom' );
		
		$item_styles = sarto_edge_get_typography_styles( 'h5' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = sarto_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = sarto_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h5'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo sarto_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_h5_styles' );
}

if ( ! function_exists( 'sarto_edge_h6_styles' ) ) {
	function sarto_edge_h6_styles() {
		$margin_top    = sarto_edge_options()->getOptionValue( 'h6_margin_top' );
		$margin_bottom = sarto_edge_options()->getOptionValue( 'h6_margin_bottom' );
		
		$item_styles = sarto_edge_get_typography_styles( 'h6' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = sarto_edge_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = sarto_edge_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h6'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo sarto_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_h6_styles' );
}

if ( ! function_exists( 'sarto_edge_text_styles' ) ) {
	function sarto_edge_text_styles() {
		$item_styles = sarto_edge_get_typography_styles( 'text' );
		
		$item_selector = array(
			'p'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo sarto_edge_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_text_styles' );
}

if ( ! function_exists( 'sarto_edge_link_styles' ) ) {
	function sarto_edge_link_styles() {
		$link_styles      = array();
		$link_color       = sarto_edge_options()->getOptionValue( 'link_color' );
		$link_font_style  = sarto_edge_options()->getOptionValue( 'link_fontstyle' );
		$link_font_weight = sarto_edge_options()->getOptionValue( 'link_fontweight' );
		$link_decoration  = sarto_edge_options()->getOptionValue( 'link_fontdecoration' );
		
		if ( ! empty( $link_color ) ) {
			$link_styles['color'] = $link_color;
		}
		if ( ! empty( $link_font_style ) ) {
			$link_styles['font-style'] = $link_font_style;
		}
		if ( ! empty( $link_font_weight ) ) {
			$link_styles['font-weight'] = $link_font_weight;
		}
		if ( ! empty( $link_decoration ) ) {
			$link_styles['text-decoration'] = $link_decoration;
		}
		
		$link_selector = array(
			'a',
			'p a'
		);
		
		if ( ! empty( $link_styles ) ) {
			echo sarto_edge_dynamic_css( $link_selector, $link_styles );
		}
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_link_styles' );
}

if ( ! function_exists( 'sarto_edge_link_hover_styles' ) ) {
	function sarto_edge_link_hover_styles() {
		$link_hover_styles     = array();
		$link_hover_color      = sarto_edge_options()->getOptionValue( 'link_hovercolor' );
		$link_hover_decoration = sarto_edge_options()->getOptionValue( 'link_hover_fontdecoration' );
		
		if ( ! empty( $link_hover_color ) ) {
			$link_hover_styles['color'] = $link_hover_color;
		}
		if ( ! empty( $link_hover_decoration ) ) {
			$link_hover_styles['text-decoration'] = $link_hover_decoration;
		}
		
		$link_hover_selector = array(
			'a:hover',
			'p a:hover'
		);
		
		if ( ! empty( $link_hover_styles ) ) {
			echo sarto_edge_dynamic_css( $link_hover_selector, $link_hover_styles );
		}
		
		$link_heading_hover_styles = array();
		
		if ( ! empty( $link_hover_color ) ) {
			$link_heading_hover_styles['color'] = $link_hover_color;
		}
		
		$link_heading_hover_selector = array(
			'h1 a:hover',
			'h2 a:hover',
			'h3 a:hover',
			'h4 a:hover',
			'h5 a:hover',
			'h6 a:hover'
		);
		
		if ( ! empty( $link_heading_hover_styles ) ) {
			echo sarto_edge_dynamic_css( $link_heading_hover_selector, $link_heading_hover_styles );
		}
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_link_hover_styles' );
}

if ( ! function_exists( 'sarto_edge_smooth_page_transition_styles' ) ) {
	function sarto_edge_smooth_page_transition_styles( $style ) {
		$id            = sarto_edge_get_page_id();
		$loader_style  = array();
		$current_style = '';
		
		$background_color = sarto_edge_get_meta_field_intersect( 'smooth_pt_bgnd_color', $id );
		if ( ! empty( $background_color ) ) {
			$loader_style['background-color'] = $background_color;
		}
		
		$loader_selector = array(
			'.edgtf-smooth-transition-loader',
			'.edgtf-sarto-spinner .edgtf-spinner-mask-default',
		);
		
		if ( ! empty( $loader_style ) ) {
			$current_style .= sarto_edge_dynamic_css( $loader_selector, $loader_style );
		}
		
		$spinner_style = array();
		$spinner_color = sarto_edge_get_meta_field_intersect( 'smooth_pt_spinner_color', $id );
		if ( ! empty( $spinner_color ) ) {
			$spinner_style['background-color'] = $spinner_color;
		}
		
		$spinner_selectors = array(
			'.edgtf-st-loader .edgtf-rotate-circles > div',
			'.edgtf-st-loader .pulse',
			'.edgtf-st-loader .double_pulse .double-bounce1',
			'.edgtf-st-loader .double_pulse .double-bounce2',
			'.edgtf-st-loader .cube',
			'.edgtf-st-loader .rotating_cubes .cube1',
			'.edgtf-st-loader .rotating_cubes .cube2',
			'.edgtf-st-loader .stripes > div',
			'.edgtf-st-loader .wave > div',
			'.edgtf-st-loader .two_rotating_circles .dot1',
			'.edgtf-st-loader .two_rotating_circles .dot2',
			'.edgtf-st-loader .five_rotating_circles .container1 > div',
			'.edgtf-st-loader .five_rotating_circles .container2 > div',
			'.edgtf-st-loader .five_rotating_circles .container3 > div',
			'.edgtf-st-loader .atom .ball-1:before',
			'.edgtf-st-loader .atom .ball-2:before',
			'.edgtf-st-loader .atom .ball-3:before',
			'.edgtf-st-loader .atom .ball-4:before',
			'.edgtf-st-loader .clock .ball:before',
			'.edgtf-st-loader .mitosis .ball',
			'.edgtf-st-loader .lines .line1',
			'.edgtf-st-loader .lines .line2',
			'.edgtf-st-loader .lines .line3',
			'.edgtf-st-loader .lines .line4',
			'.edgtf-st-loader .fussion .ball',
			'.edgtf-st-loader .fussion .ball-1',
			'.edgtf-st-loader .fussion .ball-2',
			'.edgtf-st-loader .fussion .ball-3',
			'.edgtf-st-loader .fussion .ball-4',
			'.edgtf-st-loader .wave_circles .ball',
			'.edgtf-st-loader .pulse_circles .ball'
		);
		
		if ( ! empty( $spinner_style ) ) {
			$current_style .= sarto_edge_dynamic_css( $spinner_selectors, $spinner_style );
		}
		
		$spinner_text_style = array();
		if ( ! empty( $spinner_color ) ) {
			$spinner_text_style['color'] = $spinner_color;
		}
		
		$spinner_text_selectors = array(
			'.edgtf-sarto-spinner .edgtf-sarto-spinner-title-inner',
		);
		
		if ( ! empty( $spinner_text_style ) ) {
			$current_style .= sarto_edge_dynamic_css( $spinner_text_selectors, $spinner_text_style );
		}

		$current_style = $current_style . $style;
		
		return $current_style;
	}
	
	add_filter( 'sarto_edge_add_page_custom_style', 'sarto_edge_smooth_page_transition_styles' );
}