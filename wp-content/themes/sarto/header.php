<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta name="google-site-verification" content="5RgXn_iMQ_uWI459Kpk0IkpulKGU8Yv0S04k5AbnshM" />
	<script type="application/ld+json">
		{

			"@type": "LocalBusiness",
			"name": "Wsoftpro",
			"image": "http://wsoftpro.com/wp-content/uploads/2019/08/123.png",
			"telephone": "+84 169 3558941",
			"email": "info.wsoftpro@gmail.com",
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "19 Nguyen Trai Street, Thanh Xuan.",
				"addressLocality": "Hanoi",
				"addressCountry": "Vietnam",
				"postalCode": "100000"
			},
			"openingHoursSpecification": {
				"@type": "OpeningHoursSpecification",
				"dayOfWeek": {
					"@type": "DayOfWeek",
					"name": "Monday-Saturday"
				}
			},
			"url": "https://wsoftpro.com/"
		}
	</script>
	<?php
	/**
	 * sarto_edge_header_meta hook
	 *
	 * @see sarto_edge_header_meta() - hooked with 10
	 * @see sarto_edge_user_scalable_meta - hooked with 10
	 * @see sarto_core_set_open_graph_meta - hooked with 10
	 */
	do_action('sarto_edge_header_meta');

	wp_head(); ?>
	<?php
	$schemamarkup = get_post_meta(get_the_ID(), 'schemamarkup', true);
	if (!empty($schemamarkup)) {
		echo $schemamarkup;
	}
	?>


</head>
<meta name="google-site-verification" content="5RgXn_iMQ_uWI459Kpk0IkpulKGU8Yv0S04k5AbnshM" />
<!--Start of Tawk.to Script-->
<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5dd2725643be710e1d1dd0c9/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
	})();
</script>
<!--End of Tawk.to Script-->
<body <?php body_class(); ?>>
	<?php
	/**
	 * sarto_edge_after_body_tag hook
	 *
	 * @see sarto_edge_get_side_area() - hooked with 10
	 * @see sarto_edge_smooth_page_transitions() - hooked with 10
	 */
	do_action('sarto_edge_after_body_tag'); ?>

	<div class="edgtf-wrapper">
		<div class="edgtf-wrapper-inner">
			<?php
			/**
			 * sarto_edge_after_wrapper_inner hook
			 *
			 * @see sarto_edge_get_header() - hooked with 10
			 * @see sarto_edge_get_mobile_header() - hooked with 20
			 * @see sarto_edge_back_to_top_button() - hooked with 30
			 * @see sarto_edge_get_header_minimal_full_screen_menu() - hooked with 40
			 * @see sarto_edge_get_header_bottom_navigation() - hooked with 40
			 */
			do_action('sarto_edge_after_wrapper_inner'); ?>

			<div class="edgtf-content" <?php sarto_edge_content_elem_style_attr(); ?>>
				<div class="edgtf-content-inner">