<li class="edgtf-bl-item clearfix">
	<div class="edgtf-blog-list-item-table">
		<div class="edgtf-blog-list-item-table-cell">
			<?php if ($list_title !== '') {
				echo sarto_edge_execute_shortcode('edgtf_section_title', $title_params);
			} ?>
		</div>
	</div>
	<?php if ($uncovering_animation == 'yes') : ?>
		<div class="edgtf-bli-mask edgtf-bli-mask-intro"  <?php echo sarto_edge_get_inline_style( $mask_styles['first'] ); ?>></div>
		<div class="edgtf-bli-mask edgtf-bli-mask-default" <?php echo sarto_edge_get_inline_style( $mask_styles['second'] ); ?>></div>
	<?php endif; ?>
</li>