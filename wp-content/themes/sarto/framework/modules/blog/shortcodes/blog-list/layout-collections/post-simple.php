<li class="edgtf-bl-item edgtf-item-space clearfix">
	<div class="edgtf-bli-inner">
		<?php if ( $post_info_image == 'yes' ) {
			sarto_edge_get_module_template_part( 'templates/parts/media', 'blog', '', $params );
		} ?>
		<div class="edgtf-bli-content">
			<?php sarto_edge_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params ); ?>
			<?php sarto_edge_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
			<?php sarto_edge_get_module_template_part( 'templates/parts/post-info/read-more', 'blog', '', $params ); ?>
		</div>
	</div>
</li>