<li class="edgtf-bl-item edgtf-item-space clearfix">
	<div class="edgtf-bli-inner">
		<div class="edgtf-bli-content">
			<?php sarto_edge_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params ); ?>
            <?php sarto_edge_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
            <?php sarto_edge_get_module_template_part( 'templates/parts/post-info/read-more', 'blog', '', $params ); ?>
		</div>
	</div>
</li>