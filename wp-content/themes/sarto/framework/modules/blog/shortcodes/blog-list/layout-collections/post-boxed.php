<?php $image_url = '';
$image_meta          = get_post_meta( get_the_ID(), 'edgtf_blog_list_featured_image_meta', true );
if ( ! empty( $image_meta ) ) {
    $image_url = $image_meta;
}
else {
    $image_url = get_the_post_thumbnail_url(get_the_ID(), $image_size);
}
?>

<li class="edgtf-bl-item clearfix">
    <div class="edgtf-bli-inner">
        <div class="edgtf-bli-content" style="<?php echo esc_attr($item_style); ?> background-image:url(<?php echo esc_url( $image_url ) ?>)">
            <div class="edgtf-bli-content-inner">
                <?php if ($full_screen_size == 'yes') { ?>
                    <div class="edgtf-bli-content-table-inner">
                <?php } ?>

                <?php if ($post_info_section == 'yes') { ?>
                    <div class="edgtf-bli-info">
                        <?php
                        if ($post_info_date == 'yes') {
	                        sarto_edge_get_module_template_part('templates/parts/post-info/date', 'blog', '', $params);
                        }
                        if ($post_info_category == 'yes') {
                            sarto_edge_get_module_template_part('templates/parts/post-info/category', 'blog', '', $params);
                        }
                        if ($post_info_author == 'yes') {
	                        sarto_edge_get_module_template_part('templates/parts/post-info/author', 'blog', '', $params);
                        }
                        if ($post_info_comments == 'yes') {
	                        sarto_edge_get_module_template_part('templates/parts/post-info/comments', 'blog', '', $params);
                        }
                        if ($post_info_like == 'yes') {
	                        sarto_edge_get_module_template_part('templates/parts/post-info/like', 'blog', '', $params);
                        }
                        ?>
                    </div>
                <?php } ?>
                <?php sarto_edge_get_module_template_part('templates/parts/title', 'blog', '', $params); ?>
                
                <?php if ($post_info_section == 'yes') { ?>
                    <div class="edgtf-bli-info-bottom">
	                    <?php

	                    if ($post_info_share == 'yes') {
		                    sarto_edge_get_module_template_part('templates/parts/post-info/share', 'blog', '', $params);
	                    }

                        sarto_edge_get_module_template_part( 'templates/parts/post-info/read-more', 'blog', '', $params );
	                    ?>

                    </div>
                <?php } ?>

                <?php if ($full_screen_size == 'yes') { ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</li>

