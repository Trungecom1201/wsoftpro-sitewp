<?php
namespace SartoCore\CPT\Shortcodes\BlogList;

use SartoCore\Lib;

class BlogList implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'edgtf_blog_list';
		
		add_action('vc_before_init', array($this,'vcMap'));
		
		//Category filter
		add_filter( 'vc_autocomplete_edgtf_blog_list_category_callback', array( &$this, 'blogCategoryAutocompleteSuggester', ), 10, 1 ); // Get suggestion(find). Must return an array
		
		//Category render
		add_filter( 'vc_autocomplete_edgtf_blog_list_category_render', array( &$this, 'blogCategoryAutocompleteRender', ), 10, 1 ); // Get suggestion(find). Must return an array
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map(
			array(
				'name'                      => esc_html__( 'Edge Blog List', 'sarto' ),
				'base'                      => $this->base,
				'icon'                      => 'icon-wpb-blog-list extended-custom-icon',
				'category'                  => esc_html__( 'by SARTO', 'sarto' ),
				'allowed_container_element' => 'vc_row',
				'params'                    => array(
					array(
						'type'        => 'dropdown',
						'param_name'  => 'type',
						'heading'     => esc_html__( 'Type', 'sarto' ),
						'value'       => array(
							esc_html__( 'Standard', 'sarto' ) => 'standard',
							esc_html__( 'Boxed', 'sarto' )    => 'boxed',
							esc_html__( 'Masonry', 'sarto' )  => 'masonry',
							esc_html__( 'Simple', 'sarto' )   => 'simple',
							esc_html__( 'Minimal', 'sarto' )  => 'minimal'
						),
						'save_always' => true
					),
					array(
						'type'       => 'textfield',
						'param_name' => 'number_of_posts',
						'heading'    => esc_html__( 'Number of Posts', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'full_screen_size',
						'heading'    => esc_html__( 'Full Screen Item', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array() ),
						'dependency' => array( 'element' => 'type', 'value' => array( 'boxed' ) )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'number_of_columns',
						'heading'    => esc_html__( 'Number of Columns', 'sarto' ),
						'value'      => array(
							esc_html__( 'One', 'sarto' )   => '1',
							esc_html__( 'Two', 'sarto' )   => '2',
							esc_html__( 'Three', 'sarto' ) => '3',
							esc_html__( 'Four', 'sarto' )  => '4',
							esc_html__( 'Five', 'sarto' )  => '5'
						),
						'save_always' => true,
						'dependency' => array( 'element' => 'type', 'value' => array( 'standard', 'boxed', 'masonry' ) )
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'space_between_items',
						'heading'     => esc_html__( 'Space Between Items', 'sarto' ),
						'value'       => array_flip( sarto_edge_get_space_between_items_array() ),
						'save_always' => true,
						'dependency'  => array( 'element' => 'type', 'value'   => array( 'standard', 'boxed', 'masonry' ) )
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'orderby',
						'heading'     => esc_html__( 'Order By', 'sarto' ),
						'value'       => array_flip( sarto_edge_get_query_order_by_array() ),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'order',
						'heading'     => esc_html__( 'Order', 'sarto' ),
						'value'       => array_flip( sarto_edge_get_query_order_array() ),
						'save_always' => true
					),
					array(
						'type'        => 'autocomplete',
						'param_name'  => 'category',
						'heading'     => esc_html__( 'Category', 'sarto' ),
						'description' => esc_html__( 'Enter one category slug (leave empty for showing all categories)', 'sarto' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__('List Title', 'sarto'),
						'param_name' => 'list_title',
						'dependency' => Array('element' => 'type', 'value' => array('boxed')),
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'uncovering_animation',
						'heading'    => esc_html__( 'Enable Uncovering Animation for List Title', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array(false) ),
						'dependency' => array( 'element' => 'list_title', 'not_empty' => true )
					),
                	array(
                		'type'       => 'colorpicker',
                		'param_name' => 'first_mask_color',
                		'heading'    => esc_html__( 'First Mask Color', 'sarto' ),
						'dependency'  => array( 'element' => 'uncovering_animation', 'value' => array( 'yes' ) )
                	),
                	array(
                		'type'       => 'colorpicker',
                		'param_name' => 'second_mask_color',
                		'heading'    => esc_html__( 'Second Mask Color', 'sarto' ),
						'dependency'  => array( 'element' => 'uncovering_animation', 'value' => array( 'yes' ) )
                	),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Enable SVG Underscore below List Title', 'sarto'),
						'param_name' => 'list_title_underscore',
						'value'       => array_flip(sarto_edge_get_yes_no_select_array()),
						'description' => esc_html__('Highlighted text will be appended to title', 'sarto'),
						'dependency' => Array('element' => 'list_title', 'not_empty' => true),
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'list_title_underscore_size',
						'heading'     => esc_html__('SVG Underscore Size', 'sarto'),
						'value'       => array(
							esc_html__('Normal', 'sarto') => 'normal',
							esc_html__('Large', 'sarto')  => 'large'
						),
						'save_always' => true,
						'dependency' => array('element' => 'list_title_underscore', 'value' => array('yes')),
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'image_size',
						'heading'    => esc_html__( 'Image Size', 'sarto' ),
						'value'      => array(
							esc_html__( 'Original', 'sarto' )  => 'full',
							esc_html__( 'Square', 'sarto' )    => 'sarto_edge_square',
							esc_html__( 'Landscape', 'sarto' ) => 'sarto_edge_landscape',
							esc_html__( 'Portrait', 'sarto' )  => 'sarto_edge_portrait',
							esc_html__( 'Thumbnail', 'sarto' ) => 'thumbnail',
							esc_html__( 'Medium', 'sarto' )    => 'medium',
							esc_html__( 'Large', 'sarto' )     => 'large'
						),
						'save_always' => true,
						'dependency'  => Array( 'element' => 'type', 'value' => array( 'standard', 'boxed', 'masonry' ) )
					),//
					array(
						'type'          => 'textfield',
						'param_name'    => 'content_padding',
						'heading'       => esc_html__( 'Content Area Padding', 'sarto' ),
						'description'   => esc_html__( 'Enter padding for content area in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'sarto' ),
						'group'         => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'title_tag',
						'heading'    => esc_html__( 'Title Tag', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_title_tag( true ) ),
						'group'      => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'title_transform',
						'heading'    => esc_html__( 'Title Text Transform', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_text_transform_array( true ) ),
						'group'      => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'        => 'textfield',
						'param_name'  => 'excerpt_length',
						'heading'     => esc_html__( 'Text Length', 'sarto' ),
						'description' => esc_html__( 'Number of characters', 'sarto' ),
						'dependency'  => Array( 'element' => 'type', 'value'   => array( 'standard', 'boxed', 'masonry', 'simple' ) ),
						'group'       => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_image',
						'heading'    => esc_html__( 'Enable Post Info Image', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false, true ) ),
						'dependency' => Array( 'element' => 'type', 'value'   => array( 'standard', 'boxed', 'masonry' ) ),
						'group'      => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_section',
						'heading'    => esc_html__( 'Enable Post Info Section', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false, true ) ),
						'dependency' => Array( 'element' => 'type', 'value'   => array( 'standard', 'boxed', 'masonry' ) ),
						'group'      => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_author',
						'heading'    => esc_html__( 'Enable Post Info Author', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false, true ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_date',
						'heading'    => esc_html__( 'Enable Post Info Date', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false, true ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_category',
						'heading'    => esc_html__( 'Enable Post Info Category', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false, true ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_comments',
						'heading'    => esc_html__( 'Enable Post Info Comments', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_like',
						'heading'    => esc_html__( 'Enable Post Info Like', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'post_info_share',
						'heading'    => esc_html__( 'Enable Post Info Share', 'sarto' ),
						'value'      => array_flip( sarto_edge_get_yes_no_select_array( false ) ),
						'dependency' => Array( 'element' => 'post_info_section', 'value' => array( 'yes' ) ),
						'group'      => esc_html__( 'Post Info', 'sarto' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'pagination_type',
						'heading'    => esc_html__( 'Pagination Type', 'sarto' ),
						'value'      => array(
							esc_html__( 'None', 'sarto' )            => 'no-pagination',
							esc_html__( 'Standard', 'sarto' )        => 'standard-shortcodes',
							esc_html__( 'Load More', 'sarto' )       => 'load-more',
							esc_html__( 'Infinite Scroll', 'sarto' ) => 'infinite-scroll'
						),
						'group'      => esc_html__( 'Additional Features', 'sarto' )
					)
				)
			)
		);
	}
	
	public function render( $atts, $content = null ) {
		$default_atts = array(
			'type'                  => 'standard',
			'full_screen_size'      => 'no',
			'number_of_posts'       => '-1',
			'number_of_columns'     => '3',
			'space_between_items'   => 'normal',
			'category'              => '',
			'orderby'               => 'title',
			'list_title'            => '',
			'list_title_underscore' => '',
			'list_title_underscore_size' => '',
			'uncovering_animation'	=> '',
			'first_mask_color'		=> '',
			'second_mask_color'		=> '',
			'orderby'               => 'title',
			'order'                 => 'ASC',
			'image_size'            => 'full',
			'title_tag'             => 'h4',
			'title_transform'       => '',
			'excerpt_length'        => '40',
			'content_padding'       => '',
			'post_info_section'     => 'yes',
			'post_info_image'       => 'yes',
			'post_info_author'      => 'yes',
			'post_info_date'        => 'yes',
			'post_info_category'    => 'yes',
			'post_info_comments'    => 'no',
			'post_info_like'        => 'no',
			'post_info_share'       => 'no',
			'pagination_type'       => 'no-pagination'
		);
		$params       = shortcode_atts( $default_atts, $atts );
		
		$queryArray             = $this->generateQueryArray( $params );
		$query_result           = new \WP_Query( $queryArray );
		$params['query_result'] = $query_result;
		
		$params['holder_data']    = $this->getHolderData( $params );
		$params['holder_classes'] = $this->getHolderClasses( $params, $default_atts );
		$params['title_params']   = $this->getTitleSectionParams( $params );
		$params['item_style']     = $this->getItemStyles( $params );
		$params['mask_styles'] 		 = $this->getMaskStyles( $params );
		$params['module']         = 'list';
		
		$params['max_num_pages'] = $query_result->max_num_pages;
		$params['paged']         = isset( $query_result->query['paged'] ) ? $query_result->query['paged'] : 1;
		
		$params['this_object'] = $this;
		
		ob_start();
		
		sarto_edge_get_module_template_part( 'shortcodes/blog-list/holder', 'blog', $params['type'], $params );
		
		$html = ob_get_contents();
		
		ob_end_clean();
		
		return $html;
	}
	
	public function getHolderClasses( $params, $default_atts ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['type'] ) ? 'edgtf-bl-' . $params['type'] : 'edgtf-bl-' . $default_atts['type'];
		$holderClasses[] = $this->getColumnNumberClass( $params['number_of_columns'] );
		$holderClasses[] = ! empty( $params['space_between_items'] ) ? 'edgtf-' . $params['space_between_items'] . '-space' : 'edgtf-' . $default_atts['space_between_items'] . '-space';
		$holderClasses[] = ! empty( $params['pagination_type'] ) ? 'edgtf-bl-pag-' . $params['pagination_type'] : 'edgtf-bl-pag-' . $default_atts['pagination_type'];
		$holderClasses[] = $params['full_screen_size'] == 'yes' ? 'edgtf-bl-full-screen' : '';
		$holderClasses[] = $params['uncovering_animation'] == 'yes' ? 'edgtf-bl-with-uncovering' : '';

		return implode( ' ', $holderClasses );
	}
	
	public function getColumnNumberClass( $params ) {
		switch ( $params ) {
			case 1:
				$classes = 'edgtf-bl-one-column';
				break;
			case 2:
				$classes = 'edgtf-bl-two-columns';
				break;
			case 3:
				$classes = 'edgtf-bl-three-columns';
				break;
			case 4:
				$classes = 'edgtf-bl-four-columns';
				break;
			case 5:
				$classes = 'edgtf-bl-five-columns';
				break;
			default:
				$classes = 'edgtf-bl-three-columns';
				break;
		}
		
		return $classes;
	}

	private function getItemStyles( $params ) {
		$styles = '';

		if ( $params['content_padding'] !== '' && $params['type'] === 'boxed' ) {
			$styles .= 'padding: ' . $params['content_padding'] . ';';
		}

		return $styles;
	}
	
	public function getHolderData( $params ) {
		$dataString = '';
		
		if ( get_query_var( 'paged' ) ) {
			$paged = get_query_var( 'paged' );
		} elseif ( get_query_var( 'page' ) ) {
			$paged = get_query_var( 'page' );
		} else {
			$paged = 1;
		}
		
		$query_result = $params['query_result'];
		
		$params['max_num_pages'] = $query_result->max_num_pages;
		
		if ( ! empty( $paged ) ) {
			$params['next-page'] = $paged + 1;
		}
		
		foreach ( $params as $key => $value ) {
			if ( $key !== 'query_result' && $value !== '' ) {
				$new_key = str_replace( '_', '-', $key );
				
				$dataString .= ' data-' . $new_key . '=' . esc_attr( str_replace( ' ', '', $value ) );
			}
		}
		
		return $dataString;
	}
	
	public function generateQueryArray( $params ) {
		$queryArray = array(
			'post_status'    => 'publish',
			'post_type'      => 'post',
			'orderby'        => $params['orderby'],
			'order'          => $params['order'],
			'posts_per_page' => $params['number_of_posts'],
			'post__not_in'   => get_option( 'sticky_posts' )
		);
		
		if ( ! empty( $params['category'] ) ) {
			$queryArray['category_name'] = $params['category'];
		}
		
		if ( ! empty( $params['next_page'] ) ) {
			$queryArray['paged'] = $params['next_page'];
		} else {
			$query_array['paged'] = 1;
		}
		
		return $queryArray;
	}
	
	public function getTitleStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['title_transform'] ) ) {
			$styles[] = 'text-transform: ' . $params['title_transform'];
		}
		
		return implode( ';', $styles );
	}

	/**
	 * Filter blog categories
	 *
	 * @param $query
	 *
	 * @return array
	 */
	public function blogCategoryAutocompleteSuggester( $query ) {
		global $wpdb;
		$post_meta_infos       = $wpdb->get_results( $wpdb->prepare( "SELECT a.slug AS slug, a.name AS category_title
					FROM {$wpdb->terms} AS a
					LEFT JOIN ( SELECT term_id, taxonomy  FROM {$wpdb->term_taxonomy} ) AS b ON b.term_id = a.term_id
					WHERE b.taxonomy = 'category' AND a.name LIKE '%%%s%%'", stripslashes( $query ) ), ARRAY_A );
		
		$results = array();
		if ( is_array( $post_meta_infos ) && ! empty( $post_meta_infos ) ) {
			foreach ( $post_meta_infos as $value ) {
				$data          = array();
				$data['value'] = $value['slug'];
				$data['label'] = ( ( strlen( $value['category_title'] ) > 0 ) ? esc_html__( 'Category', 'sarto' ) . ': ' . $value['category_title'] : '' );
				$results[]     = $data;
			}
		}
		
		return $results;
	}
	
	/**
	 * Find blog category by slug
	 * @since 4.4
	 *
	 * @param $query
	 *
	 * @return bool|array
	 */
	public function blogCategoryAutocompleteRender( $query ) {
		$query = trim( $query['value'] ); // get value from requested
		if ( ! empty( $query ) ) {
			// get portfolio category
			$category = get_term_by( 'slug', $query, 'category' );
			if ( is_object( $category ) ) {
				
				$category_slug = $category->slug;
				$category_title = $category->name;
				
				$category_title_display = '';
				if ( ! empty( $category_title ) ) {
					$category_title_display = esc_html__( 'Category', 'sarto' ) . ': ' . $category_title;
				}
				
				$data          = array();
				$data['value'] = $category_slug;
				$data['label'] = $category_title_display;
				
				return ! empty( $data ) ? $data : false;
			}
			
			return false;
		}
		
		return false;
	}

	public function getTitleSectionParams( $params ) {
		$title_params = array();

		if ( $params['list_title'] !== '' ) {
			$title_params['title'] = $params['list_title'];
		}

		if ( $params['list_title_underscore'] !== '' ) {
			$title_params['title_underscore'] = $params['list_title_underscore'];
		}

		if ( $params['list_title_underscore_size'] !== '' ) {
			$title_params['title_underscore_size'] = $params['list_title_underscore_size'];
		}

		return $title_params;
	}


	private function getMaskStyles( $params ) {
		$styles = array();
		$styles['first'] = '';
		$styles['second'] = '';
		
		if ( ! empty( $params['first_mask_color'] ) ) {
			$styles['first'] = 'background-color: ' . $params['first_mask_color'];
		}

		if ( ! empty( $params['second_mask_color'] ) ) {
			$styles['second'] = 'background-color: ' . $params['second_mask_color'];
		}
		
		return $styles;
	}
}