<?php if ( ! sarto_edge_post_has_read_more() && ! post_password_required() ) { ?>
	<div class="edgtf-post-read-more-button">
		<?php
			$button_params = array(
				'type'         => 'simple',
				'link'         => get_the_permalink(),
				'text'         => esc_html__( 'Explore', 'sarto' ),
				'icon_pack' => 'font_awesome',
				'fa_icon'   => 'fa-chevron-right',
				'custom_class' => 'edgtf-blog-list-button'
			);
			
			echo sarto_edge_return_button_html( $button_params );
		?>
	</div>
<?php } ?>