<?php

if ( ! function_exists( 'sarto_edge_register_blog_masonry_template_file' ) ) {
	/**
	 * Function that register blog masonry template
	 */
	function sarto_edge_register_blog_masonry_template_file( $templates ) {
		$templates['blog-masonry'] = esc_html__( 'Blog: Masonry', 'sarto' );
		
		return $templates;
	}
	
	add_filter( 'sarto_edge_register_blog_templates', 'sarto_edge_register_blog_masonry_template_file' );
}

if ( ! function_exists( 'sarto_edge_set_blog_masonry_type_global_option' ) ) {
	/**
	 * Function that set blog list type value for global blog option map
	 */
	function sarto_edge_set_blog_masonry_type_global_option( $options ) {
		$options['masonry'] = esc_html__( 'Blog: Masonry', 'sarto' );
		
		return $options;
	}
	
	add_filter( 'sarto_edge_blog_list_type_global_option', 'sarto_edge_set_blog_masonry_type_global_option' );
}