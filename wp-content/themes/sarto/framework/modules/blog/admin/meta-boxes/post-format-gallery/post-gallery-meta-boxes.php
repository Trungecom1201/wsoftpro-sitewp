<?php

if ( ! function_exists( 'sarto_edge_map_post_gallery_meta' ) ) {
	
	function sarto_edge_map_post_gallery_meta() {
		$gallery_post_format_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Gallery Post Format', 'sarto' ),
				'name'  => 'post_format_gallery_meta'
			)
		);
		
		sarto_edge_add_multiple_images_field(
			array(
				'name'        => 'edgtf_post_gallery_images_meta',
				'label'       => esc_html__( 'Gallery Images', 'sarto' ),
				'description' => esc_html__( 'Choose your gallery images', 'sarto' ),
				'parent'      => $gallery_post_format_meta_box,
			)
		);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_post_gallery_meta', 21 );
}
