<?php

if ( ! function_exists( 'sarto_edge_map_post_link_meta' ) ) {
	function sarto_edge_map_post_link_meta() {
		$link_post_format_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Link Post Format', 'sarto' ),
				'name'  => 'post_format_link_meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_post_link_link_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Link', 'sarto' ),
				'description' => esc_html__( 'Enter link', 'sarto' ),
				'parent'      => $link_post_format_meta_box
			)
		);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_post_link_meta', 24 );
}