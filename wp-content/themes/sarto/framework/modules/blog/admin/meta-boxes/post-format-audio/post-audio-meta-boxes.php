<?php

if ( ! function_exists( 'sarto_edge_map_post_audio_meta' ) ) {
	function sarto_edge_map_post_audio_meta() {
		$audio_post_format_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Audio Post Format', 'sarto' ),
				'name'  => 'post_format_audio_meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_audio_type_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Audio Type', 'sarto' ),
				'description'   => esc_html__( 'Choose audio type', 'sarto' ),
				'parent'        => $audio_post_format_meta_box,
				'default_value' => 'social_networks',
				'options'       => array(
					'social_networks' => esc_html__( 'Audio Service', 'sarto' ),
					'self'            => esc_html__( 'Self Hosted', 'sarto' )
				)
			)
		);
		
		$edgtf_audio_embedded_container = sarto_edge_add_admin_container(
			array(
				'parent' => $audio_post_format_meta_box,
				'name'   => 'edgtf_audio_embedded_container'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_post_audio_link_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Audio URL', 'sarto' ),
				'description' => esc_html__( 'Enter audio URL', 'sarto' ),
				'parent'      => $edgtf_audio_embedded_container,
				'dependency' => array(
					'show' => array(
						'edgtf_audio_type_meta' => 'social_networks'
					)
				)
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_post_audio_custom_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Audio Link', 'sarto' ),
				'description' => esc_html__( 'Enter audio link', 'sarto' ),
				'parent'      => $edgtf_audio_embedded_container,
				'dependency' => array(
					'show' => array(
						'edgtf_audio_type_meta' => 'self'
					)
				)
			)
		);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_post_audio_meta', 23 );
}