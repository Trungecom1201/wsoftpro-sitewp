<?php

if ( ! function_exists( 'sarto_edge_footer_top_general_styles' ) ) {
	/**
	 * Generates general custom styles for footer top area
	 */
	function sarto_edge_footer_top_general_styles() {
		$item_styles      = array();
		$background_color = sarto_edge_options()->getOptionValue( 'footer_top_background_color' );
		
		if ( ! empty( $background_color ) ) {
			$item_styles['background-color'] = $background_color;
		}
		
		echo sarto_edge_dynamic_css( '.edgtf-page-footer .edgtf-footer-top-holder', $item_styles );
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_footer_top_general_styles' );
}

if ( ! function_exists( 'sarto_edge_footer_bottom_general_styles' ) ) {
	/**
	 * Generates general custom styles for footer bottom area
	 */
	function sarto_edge_footer_bottom_general_styles() {
		$item_styles      = array();
		$background_color = sarto_edge_options()->getOptionValue( 'footer_bottom_background_color' );
		
		if ( ! empty( $background_color ) ) {
			$item_styles['background-color'] = $background_color;
		}
		
		echo sarto_edge_dynamic_css( '.edgtf-page-footer .edgtf-footer-bottom-holder', $item_styles );
	}
	
	add_action( 'sarto_edge_style_dynamic', 'sarto_edge_footer_bottom_general_styles' );
}