<section class="edgtf-side-menu">
	<a <?php sarto_edge_class_attribute( $side_area_close_icon_class ); ?> href="#">
		<?php echo sarto_edge_get_side_area_close_icon_html(); ?>
	</a>
	<?php if ( is_active_sidebar( 'sidearea' ) ) {
		dynamic_sidebar( 'sidearea' );
	} ?>
</section>