<?php

if ( ! function_exists( 'sarto_edge_disable_behaviors_for_header_vertical' ) ) {
	/**
	 * This function is used to disable sticky header functions that perform processing variables their used in js for this header type
	 */
	function sarto_edge_disable_behaviors_for_header_vertical( $allow_behavior ) {
		return false;
	}
	
	if ( sarto_edge_check_is_header_type_enabled( 'header-vertical', sarto_edge_get_page_id() ) ) {
		add_filter( 'sarto_edge_allow_sticky_header_behavior', 'sarto_edge_disable_behaviors_for_header_vertical' );
		add_filter( 'sarto_edge_allow_content_boxed_layout', 'sarto_edge_disable_behaviors_for_header_vertical' );
	}
}