<?php

if ( ! function_exists( 'sarto_edge_get_hide_dep_for_header_vertical_area_options' ) ) {
	function sarto_edge_get_hide_dep_for_header_vertical_area_options() {
		$hide_dep_options = apply_filters( 'sarto_edge_header_vertical_hide_global_option', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'sarto_edge_header_vertical_options_map' ) ) {
	function sarto_edge_header_vertical_options_map( $panel_header ) {
		$hide_dep_options = sarto_edge_get_hide_dep_for_header_vertical_area_options();
		
		$vertical_area_container = sarto_edge_add_admin_container_no_style(
			array(
				'parent'          => $panel_header,
				'name'            => 'header_vertical_area_container',
				'dependency' => array(
					'hide' => array(
						'header_options'  => $hide_dep_options
					)
				)
			)
		);
		
		sarto_edge_add_admin_section_title(
			array(
				'parent' => $vertical_area_container,
				'name'   => 'menu_area_style',
				'title'  => esc_html__( 'Vertical Area Style', 'sarto' )
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'        => 'vertical_header_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'sarto' ),
				'description' => esc_html__( 'Set background color for vertical menu', 'sarto' ),
				'parent'      => $vertical_area_container
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'name'          => 'vertical_header_background_image',
				'type'          => 'image',
				'default_value' => '',
				'label'         => esc_html__( 'Background Image', 'sarto' ),
				'description'   => esc_html__( 'Set background image for vertical menu', 'sarto' ),
				'parent'        => $vertical_area_container
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'parent'        => $vertical_area_container,
				'type'          => 'yesno',
				'name'          => 'vertical_header_shadow',
				'default_value' => 'no',
				'label'         => esc_html__( 'Shadow', 'sarto' ),
				'description'   => esc_html__( 'Set shadow on vertical header', 'sarto' ),
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'parent'        => $vertical_area_container,
				'type'          => 'yesno',
				'name'          => 'vertical_header_border',
				'default_value' => 'no',
				'label'         => esc_html__( 'Vertical Area Border', 'sarto' ),
				'description'   => esc_html__( 'Set border on vertical area', 'sarto' )
			)
		);
		
		$vertical_header_shadow_border_container = sarto_edge_add_admin_container(
			array(
				'parent'          => $vertical_area_container,
				'name'            => 'vertical_header_shadow_border_container',
				'dependency' => array(
					'hide' => array(
						'vertical_header_border'  => 'no'
					)
				)
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'parent'        => $vertical_header_shadow_border_container,
				'type'          => 'color',
				'name'          => 'vertical_header_border_color',
				'default_value' => '',
				'label'         => esc_html__( 'Border Color', 'sarto' ),
				'description'   => esc_html__( 'Set border color for vertical area', 'sarto' ),
			)
		);
		
		sarto_edge_add_admin_field(
			array(
				'parent'        => $vertical_area_container,
				'type'          => 'yesno',
				'name'          => 'vertical_header_center_content',
				'default_value' => 'no',
				'label'         => esc_html__( 'Center Content', 'sarto' ),
				'description'   => esc_html__( 'Set content in vertical center', 'sarto' ),
			)
		);
		
		do_action( 'sarto_edge_header_vertical_area_additional_options', $panel_header );
	}
	
	add_action( 'sarto_edge_additional_header_menu_area_options_map', 'sarto_edge_header_vertical_options_map' );
}