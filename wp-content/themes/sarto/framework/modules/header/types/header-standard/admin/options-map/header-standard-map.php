<?php

if ( ! function_exists( 'sarto_edge_get_hide_dep_for_header_standard_options' ) ) {
	function sarto_edge_get_hide_dep_for_header_standard_options() {
		$hide_dep_options = apply_filters( 'sarto_edge_header_standard_hide_global_option', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'sarto_edge_header_standard_map' ) ) {
	function sarto_edge_header_standard_map( $parent ) {
		$hide_dep_options = sarto_edge_get_hide_dep_for_header_standard_options();
		
		sarto_edge_add_admin_field(
			array(
				'parent'          => $parent,
				'type'            => 'select',
				'name'            => 'set_menu_area_position',
				'default_value'   => 'right',
				'label'           => esc_html__( 'Choose Menu Area Position', 'sarto' ),
				'description'     => esc_html__( 'Select menu area position in your header', 'sarto' ),
				'options'         => array(
					'right'  => esc_html__( 'Right', 'sarto' ),
					'left'   => esc_html__( 'Left', 'sarto' ),
					'center' => esc_html__( 'Center', 'sarto' )
				),
				'dependency' => array(
					'hide' => array(
						'header_options'  => $hide_dep_options
					)
				)
			)
		);
	}
	
	add_action( 'sarto_edge_additional_header_menu_area_options_map', 'sarto_edge_header_standard_map' );
}