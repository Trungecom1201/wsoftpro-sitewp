<?php

if ( ! function_exists( 'sarto_edge_logo_meta_box_map' ) ) {
	function sarto_edge_logo_meta_box_map() {
		
		$logo_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => apply_filters( 'sarto_edge_set_scope_for_meta_boxes', array( 'page', 'post' ), 'logo_meta' ),
				'title' => esc_html__( 'Logo', 'sarto' ),
				'name'  => 'logo_meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_logo_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Default', 'sarto' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'sarto' ),
				'parent'      => $logo_meta_box
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_logo_image_dark_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Dark', 'sarto' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'sarto' ),
				'parent'      => $logo_meta_box
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_logo_image_light_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Light', 'sarto' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'sarto' ),
				'parent'      => $logo_meta_box
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_logo_image_sticky_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Sticky', 'sarto' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'sarto' ),
				'parent'      => $logo_meta_box
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_logo_image_mobile_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Mobile', 'sarto' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'sarto' ),
				'parent'      => $logo_meta_box
			)
		);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_logo_meta_box_map', 47 );
}