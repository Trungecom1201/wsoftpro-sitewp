<?php

if ( ! function_exists( 'sarto_edge_set_search_slide_from_wt_global_option' ) ) {
    /**
     * This function set search type value for search options map
     */
    function sarto_edge_set_search_slide_from_wt_global_option( $search_type_options ) {
        $search_type_options['slide-from-window-top'] = esc_html__( 'Slide From Window Top', 'sarto' );

        return $search_type_options;
    }

    add_filter( 'sarto_edge_search_type_global_option', 'sarto_edge_set_search_slide_from_wt_global_option' );
}