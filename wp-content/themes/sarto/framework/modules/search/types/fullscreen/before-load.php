<?php

if ( ! function_exists( 'sarto_edge_set_search_fullscreen_global_option' ) ) {
    /**
     * This function set search type value for search options map
     */
    function sarto_edge_set_search_fullscreen_global_option( $search_type_options ) {
        $search_type_options['fullscreen'] = esc_html__( 'Fullscreen', 'sarto' );

        return $search_type_options;
    }

    add_filter( 'sarto_edge_search_type_global_option', 'sarto_edge_set_search_fullscreen_global_option' );
}