<?php

if ( ! function_exists( 'sarto_edge_search_body_class' ) ) {
	/**
	 * Function that adds body classes for different search types
	 *
	 * @param $classes array original array of body classes
	 *
	 * @return array modified array of classes
	 */
	function sarto_edge_search_body_class( $classes ) {
		$classes[] = 'edgtf-search-covers-header';
		
		return $classes;
	}
	
	add_filter( 'body_class', 'sarto_edge_search_body_class' );
}

if ( ! function_exists( 'sarto_edge_get_search' ) ) {
	/**
	 * Loads search HTML based on search type option.
	 */
	function sarto_edge_get_search() {
		sarto_edge_load_search_template();
	}
	
	add_action( 'sarto_edge_before_page_header_html_close', 'sarto_edge_get_search' );
	add_action( 'sarto_edge_before_mobile_header_html_close', 'sarto_edge_get_search' );
}

if ( ! function_exists( 'sarto_edge_load_search_template' ) ) {
	/**
	 * Loads search HTML based on search type option.
	 */
	function sarto_edge_load_search_template() {

		$search_in_grid   = sarto_edge_options()->getOptionValue( 'search_in_grid' ) == 'yes' ? true : false;
		
		$parameters = array(
			'search_in_grid'    		=> $search_in_grid,
			'search_close_icon_class' 	=> sarto_edge_get_search_close_icon_class()
		);
		
		sarto_edge_get_module_template_part( 'types/covers-header/templates/covers-header', 'search', '', $parameters );
	}
}