<?php

if ( ! function_exists( 'sarto_edge_map_woocommerce_meta' ) ) {
	function sarto_edge_map_woocommerce_meta() {
		
		$woocommerce_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => array( 'product' ),
				'title' => esc_html__( 'Product Meta', 'sarto' ),
				'name'  => 'woo_product_meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_product_featured_image_size',
				'type'        => 'select',
				'label'       => esc_html__( 'Dimensions for Product List Shortcode', 'sarto' ),
				'description' => esc_html__( 'Choose image layout when it appears in Edge Product List - Masonry layout shortcode', 'sarto' ),
				'options'     => array(
					''                   => esc_html__( 'Default', 'sarto' ),
					'small'              => esc_html__( 'Small', 'sarto' ),
					'large-width'        => esc_html__( 'Large Width', 'sarto' ),
					'large-height'       => esc_html__( 'Large Height', 'sarto' ),
					'large-width-height' => esc_html__( 'Large Width Height', 'sarto' )
				),
				'parent'      => $woocommerce_meta_box
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_show_title_area_woo_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'sarto' ),
				'description'   => esc_html__( 'Disabling this option will turn off page title area', 'sarto' ),
				'options'       => sarto_edge_get_yes_no_select_array(),
				'parent'        => $woocommerce_meta_box
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_show_new_sign_woo_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Show New Sign', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will show new sign mark on product', 'sarto' ),
				'parent'        => $woocommerce_meta_box
			)
		);
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_woocommerce_meta', 99 );
}