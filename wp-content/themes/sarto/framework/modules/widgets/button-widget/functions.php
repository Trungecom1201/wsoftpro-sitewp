<?php

if ( ! function_exists( 'sarto_edge_register_button_widget' ) ) {
	/**
	 * Function that register button widget
	 */
	function sarto_edge_register_button_widget( $widgets ) {
		$widgets[] = 'SartoEdgeButtonWidget';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_button_widget' );
}