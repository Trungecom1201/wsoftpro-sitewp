<?php

class SartoEdgeButtonWidget extends SartoEdgeWidget {
	public function __construct() {
		parent::__construct(
			'edgtf_button_widget',
			esc_html__( 'Edge Button Widget', 'sarto' ),
			array( 'description' => esc_html__( 'Add button element to widget areas', 'sarto' ) )
		);
		
		$this->setParams();
	}
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'    => 'dropdown',
				'name'    => 'type',
				'title'   => esc_html__( 'Type', 'sarto' ),
				'options' => array(
					'solid'   => esc_html__( 'Solid', 'sarto' ),
					'outline' => esc_html__( 'Outline', 'sarto' ),
					'simple'  => esc_html__( 'Simple', 'sarto' )
				)
			),
			array(
				'type'        => 'dropdown',
				'name'        => 'size',
				'title'       => esc_html__( 'Size', 'sarto' ),
				'options'     => array(
					'small'  => esc_html__( 'Small', 'sarto' ),
					'medium' => esc_html__( 'Medium', 'sarto' ),
					'large'  => esc_html__( 'Large', 'sarto' ),
					'huge'   => esc_html__( 'Huge', 'sarto' )
				),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'sarto' )
			),
			array(
				'type'    => 'textfield',
				'name'    => 'text',
				'title'   => esc_html__( 'Text', 'sarto' ),
				'default' => esc_html__( 'Button Text', 'sarto' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'link',
				'title' => esc_html__( 'Link', 'sarto' )
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'target',
				'title'   => esc_html__( 'Link Target', 'sarto' ),
				'options' => sarto_edge_get_link_target_array()
			),
			array(
				'type'  => 'colorpicker',
				'name'  => 'color',
				'title' => esc_html__( 'Color', 'sarto' )
			),
			array(
				'type'  => 'colorpicker',
				'name'  => 'hover_color',
				'title' => esc_html__( 'Hover Color', 'sarto' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'background_color',
				'title'       => esc_html__( 'Background Color', 'sarto' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'sarto' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'hover_background_color',
				'title'       => esc_html__( 'Hover Background Color', 'sarto' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'sarto' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'border_color',
				'title'       => esc_html__( 'Border Color', 'sarto' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'sarto' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'hover_border_color',
				'title'       => esc_html__( 'Hover Border Color', 'sarto' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'sarto' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'margin',
				'title'       => esc_html__( 'Margin', 'sarto' ),
				'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'sarto' )
			)
		);
	}
	
	public function widget( $args, $instance ) {
		$params = '';
		
		if ( ! is_array( $instance ) ) {
			$instance = array();
		}
		
		// Filter out all empty params
		$instance = array_filter( $instance, function ( $array_value ) {
			return trim( $array_value ) != '';
		} );
		
		// Default values
		if ( ! isset( $instance['text'] ) ) {
			$instance['text'] = 'Button Text';
		}
		
		// Generate shortcode params
		foreach ( $instance as $key => $value ) {
			$params .= " $key='$value' ";
		}
		
		echo '<div class="widget edgtf-button-widget">';
			echo do_shortcode( "[edgtf_button $params]" ); // XSS OK
		echo '</div>';
	}
}