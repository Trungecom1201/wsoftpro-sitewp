<?php

if ( ! function_exists( 'sarto_edge_register_recent_posts_widget' ) ) {
	/**
	 * Function that register search opener widget
	 */
	function sarto_edge_register_recent_posts_widget( $widgets ) {
		$widgets[] = 'SartoEdgeRecentPosts';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_recent_posts_widget' );
}