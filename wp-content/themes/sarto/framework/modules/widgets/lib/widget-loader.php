<?php

if ( ! function_exists( 'sarto_edge_register_widgets' ) ) {
	function sarto_edge_register_widgets() {
        $widgets = apply_filters('sarto_edge_register_widgets', $widgets = array());

        if (sarto_edge_core_plugin_installed()) {
            foreach ($widgets as $widget) {
                sarto_edge_create_wp_widget($widget);
            }
        }
	}
	
	add_action( 'widgets_init', 'sarto_edge_register_widgets' );
}