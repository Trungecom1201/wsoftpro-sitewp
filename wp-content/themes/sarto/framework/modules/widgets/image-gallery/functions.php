<?php

if ( ! function_exists( 'sarto_edge_register_image_gallery_widget' ) ) {
	/**
	 * Function that register image gallery widget
	 */
	function sarto_edge_register_image_gallery_widget( $widgets ) {
		$widgets[] = 'SartoEdgeImageGalleryWidget';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_image_gallery_widget' );
}