<?php

if ( ! function_exists( 'sarto_edge_register_separator_widget' ) ) {
	/**
	 * Function that register separator widget
	 */
	function sarto_edge_register_separator_widget( $widgets ) {
		$widgets[] = 'SartoEdgeSeparatorWidget';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_separator_widget' );
}