<?php

class SartoEdgeSideAreaOpener extends SartoEdgeWidget {
	public function __construct() {
		parent::__construct(
			'edgtf_side_area_opener',
			esc_html__( 'Edge Side Area Opener', 'sarto' ),
			array( 'description' => esc_html__( 'Display a "hamburger" icon that opens the side area', 'sarto' ) )
		);
		
		$this->setParams();
	}
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'        => 'colorpicker',
				'name'        => 'icon_color',
				'title'       => esc_html__( 'Side Area Opener Color', 'sarto' ),
				'description' => esc_html__( 'Define color for side area opener', 'sarto' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'icon_hover_color',
				'title'       => esc_html__( 'Side Area Opener Hover Color', 'sarto' ),
				'description' => esc_html__( 'Define hover color for side area opener', 'sarto' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'widget_margin',
				'title'       => esc_html__( 'Side Area Opener Margin', 'sarto' ),
				'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'sarto' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'widget_title',
				'title' => esc_html__( 'Side Area Opener Title', 'sarto' )
			)
		);
	}
	
	public function widget( $args, $instance ) {

		$side_area_icon_source 	 	= sarto_edge_options()->getOptionValue( 'side_area_icon_source' );
		$side_area_icon_pack 		= sarto_edge_options()->getOptionValue( 'side_area_icon_pack' );
		$side_area_icon_svg_path 	= sarto_edge_options()->getOptionValue( 'side_area_icon_svg_path' );

		$side_area_icon_class_array = array(
			'edgtf-side-menu-button-opener',
			'edgtf-icon-has-hover'
		);
	
		$side_area_icon_class_array[]  = $side_area_icon_source == 'icon_pack' ? 'edgtf-side-menu-button-opener-icon-pack' : 'edgtf-side-menu-button-opener-svg-path';

		$holder_styles = array();
		
		if ( ! empty( $instance['icon_color'] ) ) {
			$holder_styles[] = 'color: ' . $instance['icon_color'] . ';';
		}
		if ( ! empty( $instance['widget_margin'] ) ) {
			$holder_styles[] = 'margin: ' . $instance['widget_margin'];
		}

		?>
		
		<a <?php sarto_edge_class_attribute( $side_area_icon_class_array ); ?> <?php echo sarto_edge_get_inline_attr( $instance['icon_hover_color'], 'data-hover-color' ); ?> href="javascript:void(0)" <?php sarto_edge_inline_style( $holder_styles ); ?>>
			<?php if ( ! empty( $instance['widget_title'] ) ) { ?>
				<h5 class="edgtf-side-menu-title"><?php echo esc_html( $instance['widget_title'] ); ?></h5>
			<?php } ?>
			<span class="edgtf-side-menu-icon">
				<?php if ( ( $side_area_icon_source == 'icon_pack' ) && isset( $side_area_icon_pack ) ) {
	        		echo sarto_edge_icon_collections()->getMenuIcon( $side_area_icon_pack ); 
	        	} else if ( isset( $side_area_icon_svg_path ) ) {
                    echo sarto_edge_get_module_part( $side_area_icon_svg_path );
	            }?>
            </span>
		</a>
	<?php }
}