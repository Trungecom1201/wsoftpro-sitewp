<?php

if ( ! function_exists( 'sarto_edge_register_sidearea_opener_widget' ) ) {
	/**
	 * Function that register sidearea opener widget
	 */
	function sarto_edge_register_sidearea_opener_widget( $widgets ) {
		$widgets[] = 'SartoEdgeSideAreaOpener';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_sidearea_opener_widget' );
}