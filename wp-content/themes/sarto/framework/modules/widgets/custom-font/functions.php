<?php

if ( ! function_exists( 'sarto_edge_register_custom_font_widget' ) ) {
	/**
	 * Function that register custom font widget
	 */
	function sarto_edge_register_custom_font_widget( $widgets ) {
		$widgets[] = 'SartoEdgeCustomFontWidget';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_custom_font_widget' );
}