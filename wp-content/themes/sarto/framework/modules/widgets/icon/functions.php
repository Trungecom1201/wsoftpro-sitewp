<?php

if ( ! function_exists( 'sarto_edge_register_icon_widget' ) ) {
	/**
	 * Function that register icon widget
	 */
	function sarto_edge_register_icon_widget( $widgets ) {
		$widgets[] = 'SartoEdgeIconWidget';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_icon_widget' );
}