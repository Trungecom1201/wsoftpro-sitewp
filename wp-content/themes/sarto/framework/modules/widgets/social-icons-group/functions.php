<?php

if ( ! function_exists( 'sarto_edge_register_social_icons_widget' ) ) {
	/**
	 * Function that register social icon widget
	 */
	function sarto_edge_register_social_icons_widget( $widgets ) {
		$widgets[] = 'SartoEdgeClassIconsGroupWidget';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_social_icons_widget' );
}