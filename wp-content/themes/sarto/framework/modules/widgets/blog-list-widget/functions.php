<?php

if ( ! function_exists( 'sarto_edge_register_blog_list_widget' ) ) {
	/**
	 * Function that register blog list widget
	 */
	function sarto_edge_register_blog_list_widget( $widgets ) {
		$widgets[] = 'SartoEdgeBlogListWidget';
		
		return $widgets;
	}
	
	add_filter( 'sarto_edge_register_widgets', 'sarto_edge_register_blog_list_widget' );
}