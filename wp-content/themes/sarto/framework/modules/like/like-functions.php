<?php

if ( ! function_exists( 'sarto_edge_like' ) ) {
	/**
	 * Returns SartoEdgeLike instance
	 *
	 * @return SartoEdgeLike
	 */
	function sarto_edge_like() {
		return SartoEdgeLike::get_instance();
	}
}

function sarto_edge_get_like() {
	
	echo wp_kses( sarto_edge_like()->add_like(), array(
		'span' => array(
			'class'       => true,
			'aria-hidden' => true,
			'style'       => true,
			'id'          => true
		),
		'i'    => array(
			'class' => true,
			'style' => true,
			'id'    => true
		),
		'a'    => array(
			'href'  	   => true,
			'class' 	   => true,
			'id'    	   => true,
			'title' 	   => true,
			'style'        => true,
			'data-post-id' => true
		),
		'input' => array(
			'type'  => true,
			'name'  => true,
			'id'    => true,
			'value' => true
        )
	) );
}