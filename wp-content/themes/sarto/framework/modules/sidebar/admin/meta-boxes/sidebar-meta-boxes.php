<?php

if ( ! function_exists( 'sarto_edge_map_sidebar_meta' ) ) {
	function sarto_edge_map_sidebar_meta() {
		$edgtf_sidebar_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => apply_filters( 'sarto_edge_set_scope_for_meta_boxes', array( 'page' ), 'sidebar_meta' ),
				'title' => esc_html__( 'Sidebar', 'sarto' ),
				'name'  => 'sidebar_meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_sidebar_layout_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Sidebar Layout', 'sarto' ),
				'description' => esc_html__( 'Choose the sidebar layout', 'sarto' ),
				'parent'      => $edgtf_sidebar_meta_box,
                'options'       => sarto_edge_get_custom_sidebars_options( true )
			)
		);
		
		$edgtf_custom_sidebars = sarto_edge_get_custom_sidebars();
		if ( count( $edgtf_custom_sidebars ) > 0 ) {
			sarto_edge_create_meta_box_field(
				array(
					'name'        => 'edgtf_custom_sidebar_area_meta',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Choose Widget Area in Sidebar', 'sarto' ),
					'description' => esc_html__( 'Choose Custom Widget area to display in Sidebar"', 'sarto' ),
					'parent'      => $edgtf_sidebar_meta_box,
					'options'     => $edgtf_custom_sidebars,
					'args'        => array(
						'select2' => true
					)
				)
			);
		}
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_sidebar_meta', 31 );
}