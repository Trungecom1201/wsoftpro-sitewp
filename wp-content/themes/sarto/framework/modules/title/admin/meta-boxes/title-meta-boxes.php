<?php

if ( ! function_exists( 'sarto_edge_get_title_types_meta_boxes' ) ) {
	function sarto_edge_get_title_types_meta_boxes() {
		$title_type_options = apply_filters( 'sarto_edge_title_type_meta_boxes', $title_type_options = array( '' => esc_html__( 'Default', 'sarto' ) ) );
		
		return $title_type_options;
	}
}

foreach ( glob( EDGE_FRAMEWORK_MODULES_ROOT_DIR . '/title/types/*/admin/meta-boxes/*.php' ) as $meta_box_load ) {
	include_once $meta_box_load;
}

if ( ! function_exists( 'sarto_edge_map_title_meta' ) ) {
	function sarto_edge_map_title_meta() {
		$title_type_meta_boxes = sarto_edge_get_title_types_meta_boxes();
		
		$title_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => apply_filters( 'sarto_edge_set_scope_for_meta_boxes', array( 'page', 'post' ), 'title_meta' ),
				'title' => esc_html__( 'Title', 'sarto' ),
				'name'  => 'title_meta'
			)
		);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_show_title_area_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'sarto' ),
				'description'   => esc_html__( 'Disabling this option will turn off page title area', 'sarto' ),
				'parent'        => $title_meta_box,
				'options'       => sarto_edge_get_yes_no_select_array()
			)
		);
		
			$show_title_area_meta_container = sarto_edge_add_admin_container(
				array(
					'parent'          => $title_meta_box,
					'name'            => 'edgtf_show_title_area_meta_container',
					'dependency' => array(
						'hide' => array(
							'edgtf_show_title_area_meta' => 'no'
						)
					)
				)
			);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_type_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Area Type', 'sarto' ),
						'description'   => esc_html__( 'Choose title type', 'sarto' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => $title_type_meta_boxes
					)
				);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_in_grid_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Area In Grid', 'sarto' ),
						'description'   => esc_html__( 'Set title area content to be in grid', 'sarto' ),
						'options'       => sarto_edge_get_yes_no_select_array(),
						'parent'        => $show_title_area_meta_container
					)
				);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_title_area_height_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Height', 'sarto' ),
						'description' => esc_html__( 'Set a height for Title Area', 'sarto' ),
						'parent'      => $show_title_area_meta_container,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px'
						)
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_title_area_background_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Background Color', 'sarto' ),
						'description' => esc_html__( 'Choose a background color for title area', 'sarto' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_title_area_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'sarto' ),
						'description' => esc_html__( 'Choose an Image for title area', 'sarto' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_background_image_behavior_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Background Image Behavior', 'sarto' ),
						'description'   => esc_html__( 'Choose title area background image behavior', 'sarto' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => array(
							''                    => esc_html__( 'Default', 'sarto' ),
							'hide'                => esc_html__( 'Hide Image', 'sarto' ),
							'responsive'          => esc_html__( 'Enable Responsive Image', 'sarto' ),
							'responsive-disabled' => esc_html__( 'Disable Responsive Image', 'sarto' ),
							'parallax'            => esc_html__( 'Enable Parallax Image', 'sarto' ),
							'parallax-zoom-out'   => esc_html__( 'Enable Parallax With Zoom Out Image', 'sarto' ),
							'parallax-disabled'   => esc_html__( 'Disable Parallax Image', 'sarto' )
						)
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_vertical_alignment_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Vertical Alignment', 'sarto' ),
						'description'   => esc_html__( 'Specify title area content vertical alignment', 'sarto' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => array(
							''              => esc_html__( 'Default', 'sarto' ),
							'header-bottom' => esc_html__( 'From Bottom of Header', 'sarto' ),
							'window-top'    => esc_html__( 'From Window Top', 'sarto' )
						)
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_title_tag_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Tag', 'sarto' ),
						'options'       => sarto_edge_get_title_tag( true ),
						'parent'        => $show_title_area_meta_container
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_title_text_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Title Color', 'sarto' ),
						'description' => esc_html__( 'Choose a color for title text', 'sarto' ),
						'parent'      => $show_title_area_meta_container
					)
				);

                sarto_edge_create_meta_box_field(
                    array(
                        'name'          => 'edgtf_title_predefined_styles_meta',
                        'type'          => 'select',
                        'default_value' => '',
                        'label'         => esc_html__( 'Use our predefined Title Styles', 'sarto' ),
                        'options'       => sarto_edge_get_yes_no_select_array(),
                        'parent'        => $show_title_area_meta_container,
                    )
                );

                sarto_edge_create_meta_box_field(
                    array(
                        'name'          => 'edgtf_title_dot_meta',
                        'type'          => 'select',
                        'default_value' => '',
                        'label'         => esc_html__( 'Enable a dot after title', 'sarto' ),
                        'options'       => sarto_edge_get_yes_no_select_array(),
                        'parent'        => $show_title_area_meta_container,
                    )
                );
				
				sarto_edge_create_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_subtitle_meta',
						'type'          => 'text',
						'default_value' => '',
						'label'         => esc_html__( 'Subtitle Text', 'sarto' ),
						'description'   => esc_html__( 'Enter your subtitle text', 'sarto' ),
						'parent'        => $show_title_area_meta_container,
						'args'          => array(
							'col_width' => 6
						)
					)
				);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'          => 'edgtf_title_area_subtitle_tag_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Subtitle Tag', 'sarto' ),
						'options'       => sarto_edge_get_title_tag( true, array( 'p' => 'p' ) ),
						'parent'        => $show_title_area_meta_container
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_subtitle_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Subtitle Color', 'sarto' ),
						'description' => esc_html__( 'Choose a color for subtitle text', 'sarto' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
		/***************** Additional Title Area Layout - start *****************/
		
		do_action( 'sarto_edge_additional_title_area_meta_boxes', $show_title_area_meta_container );
		
		/***************** Additional Title Area Layout - end *****************/
		
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_title_meta', 60 );
}