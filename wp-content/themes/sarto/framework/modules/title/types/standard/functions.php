<?php

if ( ! function_exists( 'sarto_edge_set_title_standard_type_for_options' ) ) {
	/**
	 * This function set standard title type value for title options map and meta boxes
	 */
	function sarto_edge_set_title_standard_type_for_options( $type ) {
		$type['standard'] = esc_html__( 'Standard', 'sarto' );
		
		return $type;
	}
	
	add_filter( 'sarto_edge_title_type_global_option', 'sarto_edge_set_title_standard_type_for_options' );
	add_filter( 'sarto_edge_title_type_meta_boxes', 'sarto_edge_set_title_standard_type_for_options' );
}

if ( ! function_exists( 'sarto_edge_set_title_standard_type_as_default_options' ) ) {
	/**
	 * This function set default title type value for global title option map
	 */
	function sarto_edge_set_title_standard_type_as_default_options( $type ) {
		$type = 'standard';
		
		return $type;
	}
	
	add_filter( 'sarto_edge_default_title_type_global_option', 'sarto_edge_set_title_standard_type_as_default_options' );
}