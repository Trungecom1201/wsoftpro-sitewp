<?php

if ( ! function_exists( 'sarto_edge_breadcrumbs_title_type_options_meta_boxes' ) ) {
	function sarto_edge_breadcrumbs_title_type_options_meta_boxes( $show_title_area_meta_container ) {
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_breadcrumbs_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Breadcrumbs Color', 'sarto' ),
				'description' => esc_html__( 'Choose a color for breadcrumbs text', 'sarto' ),
				'parent'      => $show_title_area_meta_container
			)
		);
	}
	
	add_action( 'sarto_edge_additional_title_area_meta_boxes', 'sarto_edge_breadcrumbs_title_type_options_meta_boxes' );
}