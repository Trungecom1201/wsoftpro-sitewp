<?php

if ( ! function_exists( 'sarto_edge_map_general_meta' ) ) {
	function sarto_edge_map_general_meta() {
		
		$general_meta_box = sarto_edge_create_meta_box(
			array(
				'scope' => apply_filters( 'sarto_edge_set_scope_for_meta_boxes', array( 'page', 'post' ), 'general_meta' ),
				'title' => esc_html__( 'General', 'sarto' ),
				'name'  => 'general_meta'
			)
		);
		
		/***************** Slider Layout - begin **********************/
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_page_slider_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Slider Shortcode', 'sarto' ),
				'description' => esc_html__( 'Paste your slider shortcode here', 'sarto' ),
				'parent'      => $general_meta_box
			)
		);
		
		/***************** Slider Layout - begin **********************/
		
		/***************** Content Layout - begin **********************/
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_page_content_behind_header_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Always put content behind header', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will put page content behind page header', 'sarto' ),
				'parent'        => $general_meta_box
			)
		);
		
		$edgtf_content_padding_group = sarto_edge_add_admin_group(
			array(
				'name'        => 'content_padding_group',
				'title'       => esc_html__( 'Content Style', 'sarto' ),
				'description' => esc_html__( 'Define styles for Content area', 'sarto' ),
				'parent'      => $general_meta_box
			)
		);
		
			$edgtf_content_padding_row = sarto_edge_add_admin_row(
				array(
					'name'   => 'edgtf_content_padding_row',
					'next'   => true,
					'parent' => $edgtf_content_padding_group
				)
			);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'   => 'edgtf_page_content_padding',
						'type'   => 'textsimple',
						'label'  => esc_html__( 'Content Padding (e.g. 10px 5px 10px 5px)', 'sarto' ),
						'parent' => $edgtf_content_padding_row,
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'name'    => 'edgtf_page_content_padding_mobile',
						'type'    => 'textsimple',
						'label'   => esc_html__( 'Content Padding for mobile (e.g. 10px 5px 10px 5px)', 'sarto' ),
						'parent'  => $edgtf_content_padding_row,
					)
				);
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_page_background_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Page Background Color', 'sarto' ),
				'description' => esc_html__( 'Choose background color for page content', 'sarto' ),
				'parent'      => $general_meta_box
			)
		);
		
		/***************** Content Layout - end **********************/
		
		/***************** Boxed Layout - begin **********************/
		
		sarto_edge_create_meta_box_field(
			array(
				'name'    => 'edgtf_boxed_meta',
				'type'    => 'select',
				'label'   => esc_html__( 'Boxed Layout', 'sarto' ),
				'parent'  => $general_meta_box,
				'options' => sarto_edge_get_yes_no_select_array()
			)
		);
		
			$boxed_container_meta = sarto_edge_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'boxed_container_meta',
					'dependency' => array(
						'hide' => array(
							'edgtf_boxed_meta'  => array('','no')
						)
					)
				)
			);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_page_background_color_in_box_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Page Background Color', 'sarto' ),
						'description' => esc_html__( 'Choose the page background color outside box', 'sarto' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_boxed_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'sarto' ),
						'description' => esc_html__( 'Choose an image to be displayed in background', 'sarto' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_boxed_pattern_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Pattern', 'sarto' ),
						'description' => esc_html__( 'Choose an image to be used as background pattern', 'sarto' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'name'          => 'edgtf_boxed_background_image_attachment_meta',
						'type'          => 'select',
						'default_value' => 'fixed',
						'label'         => esc_html__( 'Background Image Attachment', 'sarto' ),
						'description'   => esc_html__( 'Choose background image attachment', 'sarto' ),
						'parent'        => $boxed_container_meta,
						'options'       => array(
							''       => esc_html__( 'Default', 'sarto' ),
							'fixed'  => esc_html__( 'Fixed', 'sarto' ),
							'scroll' => esc_html__( 'Scroll', 'sarto' )
						)
					)
				);
		
		/***************** Boxed Layout - end **********************/
		
		/***************** Passepartout Layout - begin **********************/
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_paspartu_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Passepartout', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will display passepartout around site content', 'sarto' ),
				'parent'        => $general_meta_box,
				'options'       => sarto_edge_get_yes_no_select_array(),
			)
		);
		
			$paspartu_container_meta = sarto_edge_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'edgtf_paspartu_container_meta',
					'dependency' => array(
						'hide' => array(
							'edgtf_paspartu_meta'  => array('','no')
						)
					)
				)
			);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_paspartu_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Passepartout Color', 'sarto' ),
						'description' => esc_html__( 'Choose passepartout color, default value is #ffffff', 'sarto' ),
						'parent'      => $paspartu_container_meta
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_paspartu_width_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Passepartout Size', 'sarto' ),
						'description' => esc_html__( 'Enter size amount for passepartout', 'sarto' ),
						'parent'      => $paspartu_container_meta,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_paspartu_responsive_width_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Responsive Passepartout Size', 'sarto' ),
						'description' => esc_html__( 'Enter size amount for passepartout for smaller screens (tablets and mobiles view)', 'sarto' ),
						'parent'      => $paspartu_container_meta,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
				
				sarto_edge_create_meta_box_field(
					array(
						'parent'        => $paspartu_container_meta,
						'type'          => 'select',
						'default_value' => '',
						'name'          => 'edgtf_disable_top_paspartu_meta',
						'label'         => esc_html__( 'Disable Top Passepartout', 'sarto' ),
						'options'       => sarto_edge_get_yes_no_select_array(),
					)
				);
		
				sarto_edge_create_meta_box_field(
					array(
						'parent'        => $paspartu_container_meta,
						'type'          => 'select',
						'default_value' => '',
						'name'          => 'edgtf_enable_fixed_paspartu_meta',
						'label'         => esc_html__( 'Enable Fixed Passepartout', 'sarto' ),
						'description'   => esc_html__( 'Enabling this option will set fixed passepartout for your screens', 'sarto' ),
						'options'       => sarto_edge_get_yes_no_select_array(),
					)
				);
		
		/***************** Passepartout Layout - end **********************/
		
		/***************** Content Width Layout - begin **********************/
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_initial_content_width_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Initial Width of Content', 'sarto' ),
				'description'   => esc_html__( 'Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid")', 'sarto' ),
				'parent'        => $general_meta_box,
				'options'       => array(
					''                => esc_html__( 'Default', 'sarto' ),
					'edgtf-grid-1100' => esc_html__( '1100px', 'sarto' ),
					'edgtf-grid-1300' => esc_html__( '1300px', 'sarto' ),
					'edgtf-grid-1200' => esc_html__( '1200px', 'sarto' ),
					'edgtf-grid-1000' => esc_html__( '1000px', 'sarto' ),
					'edgtf-grid-800'  => esc_html__( '800px', 'sarto' )
				)
			)
		);
		
		/***************** Content Width Layout - end **********************/
		
		/***************** Smooth Page Transitions Layout - begin **********************/
		
		sarto_edge_create_meta_box_field(
			array(
				'name'          => 'edgtf_smooth_page_transitions_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Smooth Page Transitions', 'sarto' ),
				'description'   => esc_html__( 'Enabling this option will perform a smooth transition between pages when clicking on links', 'sarto' ),
				'parent'        => $general_meta_box,
				'options'       => sarto_edge_get_yes_no_select_array()
			)
		);
		
			$page_transitions_container_meta = sarto_edge_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'page_transitions_container_meta',
					'dependency' => array(
						'hide' => array(
							'edgtf_smooth_page_transitions_meta'  => array('','no')
						)
					)
				)
			);
		
				sarto_edge_create_meta_box_field(
					array(
						'name'        => 'edgtf_page_transition_preloader_meta',
						'type'        => 'select',
						'label'       => esc_html__( 'Enable Preloading Animation', 'sarto' ),
						'description' => esc_html__( 'Enabling this option will display an animated preloader while the page content is loading', 'sarto' ),
						'parent'      => $page_transitions_container_meta,
						'options'     => sarto_edge_get_yes_no_select_array()
					)
				);
				
				$page_transition_preloader_container_meta = sarto_edge_add_admin_container(
					array(
						'parent'          => $page_transitions_container_meta,
						'name'            => 'page_transition_preloader_container_meta',
						'dependency' => array(
							'hide' => array(
								'edgtf_page_transition_preloader_meta'  => array('','no')
							)
						)
					)
				);
				
					sarto_edge_create_meta_box_field(
						array(
							'name'   => 'edgtf_smooth_pt_bgnd_color_meta',
							'type'   => 'color',
							'label'  => esc_html__( 'Page Loader Background Color', 'sarto' ),
							'parent' => $page_transition_preloader_container_meta
						)
					);
					
					$group_pt_spinner_animation_meta = sarto_edge_add_admin_group(
						array(
							'name'        => 'group_pt_spinner_animation_meta',
							'title'       => esc_html__( 'Loader Style', 'sarto' ),
							'description' => esc_html__( 'Define styles for loader spinner animation', 'sarto' ),
							'parent'      => $page_transition_preloader_container_meta
						)
					);
					
					$row_pt_spinner_animation_meta = sarto_edge_add_admin_row(
						array(
							'name'   => 'row_pt_spinner_animation_meta',
							'parent' => $group_pt_spinner_animation_meta
						)
					);
					
					sarto_edge_create_meta_box_field(
						array(
							'type'    => 'selectsimple',
							'name'    => 'edgtf_smooth_pt_spinner_type_meta',
							'label'   => esc_html__( 'Spinner Type', 'sarto' ),
							'parent'  => $row_pt_spinner_animation_meta,
							'options' => array(
								''                      => esc_html__( 'Default', 'sarto' ),
								'text_loader'        	=> esc_html__( 'Text Loader', 'sarto' ),
								'rotate_circles'        => esc_html__( 'Rotate Circles', 'sarto' ),
								'pulse'                 => esc_html__( 'Pulse', 'sarto' ),
								'double_pulse'          => esc_html__( 'Double Pulse', 'sarto' ),
								'cube'                  => esc_html__( 'Cube', 'sarto' ),
								'rotating_cubes'        => esc_html__( 'Rotating Cubes', 'sarto' ),
								'stripes'               => esc_html__( 'Stripes', 'sarto' ),
								'wave'                  => esc_html__( 'Wave', 'sarto' ),
								'two_rotating_circles'  => esc_html__( '2 Rotating Circles', 'sarto' ),
								'five_rotating_circles' => esc_html__( '5 Rotating Circles', 'sarto' ),
								'atom'                  => esc_html__( 'Atom', 'sarto' ),
								'clock'                 => esc_html__( 'Clock', 'sarto' ),
								'mitosis'               => esc_html__( 'Mitosis', 'sarto' ),
								'lines'                 => esc_html__( 'Lines', 'sarto' ),
								'fussion'               => esc_html__( 'Fussion', 'sarto' ),
								'wave_circles'          => esc_html__( 'Wave Circles', 'sarto' ),
								'pulse_circles'         => esc_html__( 'Pulse Circles', 'sarto' )
							)
						)
					);
					
					sarto_edge_create_meta_box_field(
						array(
							'type'   => 'colorsimple',
							'name'   => 'edgtf_smooth_pt_spinner_color_meta',
							'label'  => esc_html__( 'Spinner Color', 'sarto' ),
							'parent' => $row_pt_spinner_animation_meta
						)
					);
					
					sarto_edge_create_meta_box_field(
						array(
							'name'        => 'edgtf_page_transition_fadeout_meta',
							'type'        => 'select',
							'label'       => esc_html__( 'Enable Fade Out Animation', 'sarto' ),
							'description' => esc_html__( 'Enabling this option will turn on fade out animation when leaving page', 'sarto' ),
							'options'     => sarto_edge_get_yes_no_select_array(),
							'parent'      => $page_transitions_container_meta
						
						)
					);
		
		/***************** Smooth Page Transitions Layout - end **********************/
		
		/***************** Comments Layout - begin **********************/
		
		sarto_edge_create_meta_box_field(
			array(
				'name'        => 'edgtf_page_comments_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Show Comments', 'sarto' ),
				'description' => esc_html__( 'Enabling this option will show comments on your page', 'sarto' ),
				'parent'      => $general_meta_box,
				'options'     => sarto_edge_get_yes_no_select_array()
			)
		);
		
		/***************** Comments Layout - end **********************/
	}
	
	add_action( 'sarto_edge_meta_boxes_map', 'sarto_edge_map_general_meta', 10 );
}